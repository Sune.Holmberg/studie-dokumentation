# Logbog

12/09/2023
- Møde med Nicolaj, omkring forventninger til faget, samt nogle fif og standarder (NIS2) som eventuelt vil kunne bruges til at undersøge ot-sikkerhed.

18/09/2023
- Udlån af en Siemens LOGO!, som vi ville kunne bruge til at teste ting på. Ud over det har vi også fået muligheden for at kunne arbejde i Tekniklab med en anden plc, som kan meget mere, end den plc som vi har fået udlånt. 