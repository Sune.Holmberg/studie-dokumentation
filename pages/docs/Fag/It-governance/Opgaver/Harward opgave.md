# Harward opgaven

## Hvad synes du om Target angrebet? Var de særligt udsat eller var de bare uheldige?
- Trusselsaltøren have udset sig Target's julesalg som et motiv, da de vidste der ville være mange transaktion som de ville kunne anskaffe sig data omkring 

## Hvad kunne Target have gjort bedre for at undgå angrebet. Var der nogle Tekniske og eller organisatoriske tiltag Target kunne have i gang sat?
- De kunne have lavet en bedre infrastruktur
- De kunne lave flere test/stikprøver af systemet 

## Hvad synes du om cybersikkerheds organisationen? Hvad er de stræke og svage sider?
- De var blivet for sikker på sin sag, og de havde alt for mange falske positiver, som gik ud over deres dømmekræft 
- De havde også opsat deres firewallregler forkert, hvilket der komprimenterede hele infrastrukturen på netværket

## Hvad synes du om Target's første reaktion? Hvad gjorde Target godt og hvad kunne have været bedre?
- De kunne have taget alerts omkring malware mere seriøst, og derved reageret bedre på trusslen 
- Jeg synes de handlede forkert, ved ikke at tage det seriøst

## Hvad tror du den manglede reaktion på FireEys alarmer skyldes?
- Inkompetente medarbejere
- For meget arbejde, for lidt tid 

## Kan Target's bestyrelse gøres ansvarlige for bruddet og de konsekvenser det medførte både for kunderne, men også for Target selv? Hvis du var en del af bestyrelsen i Target hvad ville du have gjort? Hvilke ændringer vil du forslå? 
- Ja, for det er i sidste ende bestyrelsen som har ansvaret for virksomheden
- Jeg havde skabt mere opmærksomhed omkring it-sikkerhed, og sagt at man skal reagere på alarmer om det er falske positiver eller ej 

## Kan direktøren alene holdes ansvarlig for dette brud?
- Både og, det er ham som i sidste ende bestemmer og sidder med ansvart

## Hvad mener du om at direktøren ikke blev informeret, i 3 dage, efter at FBI var gået ind i sagen?
- Jeg mere der er en fejl fra bestyrelsens side
- Når ting som dette sker, skal det raporteres til den øverste ledelse som det første, for det er dem som sidder med ansvaret 

## Hvad kan vi lære fra denne sag? Her tænkes der på kommunikation og it-sikkerhed
- Der skal være et større fokus på alarmer 
- Man skal ikke tro at alting er falske positiver 
- Man skal have en incedent responce plan (Hvad man skal gøre hvis uheldet er ude)

## Hvad mener du at årsagen til at Target blev hackede?
- Uopmærksomme medarbejder, som ikke ved hvordan de skulle reagere på alarmerne 

## Hvilke større fejl begik Target, da de først blev bekendt med angrebet?
- At de ikke gjorde noget ved det, at de ikke fik isoleret hvad problemet var, og på den måde kunne mindske skaden 

## Hvis du ejede aktier i Target ville du så på generalforsamlinger stemme for at genvælge direktør og bestyrelsen?
- Jeg ville stemme for at komme af med dem, da det er dem som har ansvaret 

## Hvad tror du at firmaer kan gøre bedre i dag for at beskytte dem selv fra at blive hacked og måde de kommunikere på? 
- Større fokus på sikkerheden 
- Multifaktor authentication
- End to end krypteret kommunikation, så man in the middle ikke vil kunne få fat i den kommunikeret data