# Opgave 1 - ID-Kort


## Pros
- Man bekrædter sin identitet 
- Man kan have mere sikkerhed ved indgangen

## Cons
- Hvis det bliver stjålet, kan de stæle ens identitet lettere
- Man går rundt med oplysninger om sig selv i et åbet rum

## En risikovurdering
- For mange oplysninger på kortet -> S = 1 K = 4 
- Information omkring medarbejdere ligger i databasen -> S = 5, K = 4
- Kortet bliver stjålet -> S = 2, K = 4
- Kortet giver for meget adgang -> S = 3, K = 4 

## En vurdering af løsningsforslaget i forhold til GDPR
- Begræns hvad for nogle oplysninger som står på kortet
- God systemsikkerhed, og mulighed for at kunne slette oplysninger 
- 2-faktors authentication 

## "Buisness Impact Asscssment" - Analyse (BIA)


## Trusselsregister
