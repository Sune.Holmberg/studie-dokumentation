# Noter

## Dataansvarlig vs databehandler
- Dataansvarlig er ansvarlig for dataen, og bestemmer hvordan og hvad databehandleren må bruge dataen til
- databehandler arbejder med dataen, men må kun bruge det, som dataansvarlig har sagt det må bruges til.

## Hvis det ikke er skrevet ned, er det ikke sket

## Man kan ikke blive certiferet i CIS, men de ligger et godt grundlag for revideringer
- CIS er noget man kan tage elementer fra, men man behøver ikke bruge det hele
- Den er kontrol basseret, ikke risiko basseret 

## Eventuelt reflekter til Familiesamhuset eller nævnehus til eksamen 
- Der sker tid problemer (eks. datalæk og persondata som kommer ud)