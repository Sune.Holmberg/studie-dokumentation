# Noter

## OSI-Modellen 
![OSI-Model](https://shardeum.org/blog/wp-content/uploads/2022/09/The-Physical-Layer-in-OSI-Model-Explained-thumbnail.jpg)

## CIS - Center for internet security 
https://www.cisecurity.org/

Best practies for internet security. Det er ikke en standard. 

## NTP - Netværk time protocol  
Synkronicering af tiden på en server/computer.
NTP bruges til at sikre at at tidspunkterne er ens på tværs af systemer.

## Grafana 
https://grafana.com/

Bruges til at visualisere grafer for eventuelt servere. 

## IPERF
Bruges til at tjekke netværk, for at finde frem til bottleneck i systemet

## DRAW.IO 
Kan bruges til at tegne modeller og diagrammer 

## Fysisk diagram / Model
Fysiske diagrammer fortæller om hvordan man fysisk har forbundet kablerne rundt i netværket.

## Logisk diagram / Model
Logiske diagrammer fortæller om hvordan man logisk har forbundet forskellige enheder på et netværk. Typisk fortæller man om ip-adresserne på de forskellige subnets.

## Subnet
- Subnet er, som navnet antyder, et slags undernet af et større net. Der findes i dag millioner af forskellige netværk verden over. Nogen er større end andre og er derfor svære at administrere og have overblik over. Derfor opdeles disse netværk i mindre undernet (subnet) for at opnå flere forskellige fordel som for eksempel:
- Øget sikkerhed ved, at et subnet ikke kan få adgang til et andet subnet
- Mindre netværkstrafik
- Subnet kan blive vist på flere forskellige måder
    - 255.255.255.0 -> decimal
    - 11111111.11111111.111111111.00000000 -> binær
    - /24 -> CIDA notation

## Switches
- En switch er et stykke hardware, som bruges til at fordele et netværk, når man bruger en switch bliver pakker sendt til de rigtige pc'er.
- Hver maskine har sin egen streng/port til switchen

## Router
- En router er et stykke hardware, som sikre at pakkerne bliver sendt ud til de rigtige ip-adresser på tværs af internettet.

## IP-adresse
- En ip-adresse, er den adresse, som er blevet tildelt til den enkelte computer på netværket. 
- Der findes IPv4 som eks. er 192.168.111.1 og IPv6 som eks. er 3002:0bd6:0000:0000:0000:ee00:0033:6778

## Mac-adresse
- Mac-adresse er den fysiske adresse, som et stykke hardware har, den er unik for alle maskiner.

## VLANs
- VLAN, er et virtuelt loacal area network, hvor man sætter alle pc'erne i en grupper, hvor alle har adgang til alt trafikken, som forgår over lan netværket. 

### Untagged 
Det betyder at man ingen vlan information får. 

### Trunking/Tagged

## DHCP 
Dynamic Host Configuration Protocol (DHCP) det er en client/server protocol som automatisk giver en Internet Protocol (IP) host med dens IP address og andre relateret konfigurationer information som subnet mask og default gateway.


## Asymetisk kryptering
- Bedre kryptering
- Langsomere

## Symetisk kryptering
- Meget hurtigt 
- Samme nøgler i begge ender

## Client/server - Cetificate Authority

## ISP - Internet Service Provider
- Den enhed som man får udleveret af sin internet provider (eks. yousee)

## Passiv DNS 
- Det er en database, som passivt lytter på trafikken 

## Aktiv DNS 
- Man eksponere sig selv til systemer

## PCAP
- Det er en optagelse af netværkstrafik, over en given periode, hvor man kan dykke mere ned i filen, blandt andet igennem Wireshark

## Gnu core utilities 
- https://www.gnu.org/software/coreutils/

## End-to-end encryption
- Hele netværksstrømmen er krypteret 

## Dig utility 
- Laver en DNS request
- https://linux.die.net/man/1/dig

## ARP Spoofing

## DHCP Spoofing

## IP Spoofing

## DNS og domain issues

## MAC SEC

## 802.1x

## Symetrisk kryptering

## Asymestisk kryptering

## Fundamentet i sikkerhed
- Stoler jeg på den jeg snakker med?

## Stateless
- Uden hukommelse

## Statefull
- Med hukommelse 

## PKI -> Public key infrastructur 
- Internt i firmaet 
- Password på en private key er en meget god ide
- Træstruktur, et certifikat signeres af et andet certifikat, og i sidste ende af et root certificate

## CSR -> Certificate signing request
- Public/Private key bliver lavet på serveren, og sender en request til certificate authorities om at blive signeret 

## Certificate checklist
- Ikke udløbet 
- Hostname matcher
- Trustet signering (Korrekte public-/private keys)

## Certificate pinning (Sikring mod MITM med certificate authorities certifikater)

## Downgrade attacks
- Man får brugeren til at bruge en svagere version af eks. krypteringsalgoritmer 