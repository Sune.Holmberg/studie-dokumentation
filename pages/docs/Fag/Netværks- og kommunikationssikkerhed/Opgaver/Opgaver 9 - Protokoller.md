# Opgave 9 - Protokoller

## Instruktioner
1. Find to-tre protokoller fra wiresharks test pakker som du kan genkende
2. Forstå hvad der foregår, evt. vha. google

### 1

- En af pakkerne er "HTTP.CAP", hvor man ved brug af wireshark, kan se hele html koden for siden, da den bliver sendt med plain text. 

- En anden pakke, er det første under TCP. I denne pakke kan med brug af wireshark, få generet hvad der stå i den pakke som er blevet sendt, ved at følge tcp-streamen. Pakkens indhold skaber beskeden "Hello" 