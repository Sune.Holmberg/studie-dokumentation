# Opgave 14 - Wireshark basic

1. Noget med at hente en VM med en webserver, måske en raspberry?
- "Skal bare glemmes" Mortens ord

2. Start wireshark

3. Gå ind på http http://mirror.one.com/debian/ og brug din browsers view source

4. Genfind trafikken i wireshark, brug follow tcp stream

5. Hent den samme side via https https://mirror.one.com/debian/

6. Genfind trafikken i wireshark, brug follow tcp stream

7. Brug curl/wget fra kommando linien til at hente HTTP fra linket i punkt 3

8. Sammenlign med resultatet fra browserens view source