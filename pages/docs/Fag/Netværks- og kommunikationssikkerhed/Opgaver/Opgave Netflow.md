# Opgave - Netflow
1. Installation samt opsætning 
- Download [debian](https://www.debian.org/distrib/netinst) -
 Det er meget vigtigt at det ikke er i en kali maskine, fordi det virker ikke
- Installer en debian 11.x maskine i vmware. 
- Følg [guiden](https://packages.ntop.org/apt-stable/) for den version man bruger

- Man kan også installere det på en ubuntu maskine

2. På ubuntu maskinen kan man følge [guiden](https://www.how2shout.com/linux/how-to-install-ntopng-on-ubuntu-22-04-lts-jammy/#3_Install_NTopng_for_Ubuntu_2204)

3. Når maskinen er blevet installeret, kan man tilgå Ntopng fra en browser ved at skrive ip-adressen samt porten i url'et