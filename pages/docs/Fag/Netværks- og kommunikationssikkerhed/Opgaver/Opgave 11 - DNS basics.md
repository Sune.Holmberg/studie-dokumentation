# Instruktioner
1. Gennemfør rummet https://tryhackme.com/room/dnsindetail
2. Skriv dine opdagelser ned undervejs og dokumenter det på gitlab
3. Besvar herefter følgende om DNS og dokumenter undervejs:

    - Forklar med dine egne ord hvad DNS er og hvad det bruges til.
        - DNS er Domain Name System, og det bruges til at lave kommunikation mellem devices på internettet, unden at den skal huske på bla. ip-adresser
        - DNS oversætter fra domain name ip-adresser

    - Hvilke dele af CIA er aktuelle og hvordan kan DNS sikres? https://blog.cloudflare.com/dns-encryption-explained/
        - DNS, har stor betydning for integriteten i CIA-modellen, fordi hvis en dns-server er komprimeret, kan man ikke stole på at den information man får er korrekt.
        - DNS kan sikres via en firewall, som man kan sætte regler på, som eks. at ip'er fra et bestemt land ikke må tilgå dns-serveren

    - Hvilken port benytter DNS ?
        - DNS benytter sig af port 53

    - Beskriv DNS domæne hierakiet
        - Root Domain (.)
        - Top-level Domain (.edu, .com, .dk) 
        - Second-Level domain (det før eks. .dk)
        - Subdomain (hvis der står noget før second-level domain (eks. hvis der står admin.ucl.dk))

    - Forklar disse DNS records: A, AAAA, MX, TXT og CNAME.
        - A -> IPv4 adresser eks. 104.26.10.229
        - AAAA -> IPv6 adresser eks. 2606:4700:20::681a:be5
        - MX -> Adressen til den server som håndtere mails for det domain som man prøver at tilgå
        - TXT -> Textfields som indholder tekst 
        - CNAME -> Det giver et andet domain alias

        ![text](../../../billeder/Netv%C3%A6rk/DNS%20Record%20Types.png)

    - Brug nslookup til at undersøge hvor mange mailservere ucl.dk har
        - Der er 3. mail120.ucl.dk, mail121.ucl.dk og mail122.ucl.dk
        ![mx](../../../billeder/Netv%C3%A6rk/ucl%20mx%20dns.png)

    - Brug dig til at finde txt records for ucl.dk

        ![txt](../../../billeder/Netv%C3%A6rk/ucl%20txt%20dns.png)

    - Brug wireshark til:

        -  Filtrere DNS trafik. Dokumenter hvilket filter du skal bruge for kun at se DNS trafik
            - For at kunne se DNS trafik, kan man bruge "DNS" som filter.

        - Lav screenshots af eksempler på DNS trafik
            ![DNS-Search](../../../billeder/Netv%C3%A6rk/Wireshark%20dns.png)
            ![UPD-Stream](../../../billeder/Netv%C3%A6rk/wireshark%20dns%20udpstream.png)