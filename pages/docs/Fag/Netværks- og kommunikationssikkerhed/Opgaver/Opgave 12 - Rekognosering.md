# Instruktioner

1. Gennemfør THM rummet: https://tryhackme.com/room/passiverecon

2. Læs om og registrer en konto på securitytrails.com

3. Beskriv forskellen på aktiv og passiv rekognoscering
- Aktiv -> Det er hvor man prøver at tilgå information, hvor man kan komme til at alarmere systemerne/personerne 
- Passiv -> Det er hvor man tilgår information, uden risiko for at alarmere systemerne/personerne

4. Lav en liste med de værktøjer du kender til aktiv rekognoscering, beskriv kort deres formål
- NMAP -> Bruges til at mappe netværket 
- Metasploit -> Værktøj til penetrering testning
- Dig -> 
- NSLookup -> 

5. Lav en liste med de værktøjer du kender til passiv rekognoscering, beskriv kort deres formål
- Shodan.io -> Værktøj til at søge på servere som er forbundet til internettet
- WireShark -> Værktøj til at opsnappe netværkstrafik 

6. Beskriv med dine egne ord hvad en fjendtlig aktør kan bruge rekognoscering til
- De kan opsnappe information, omkring eventuelle huller de kan misbruge for at komme ind i systemet

7. Beskriv med dine egne ord hvad du kan bruge rekognoscering til, når du skal arbejde med sikkerhed
- Man kan bruge det til at se efter om der eventuelt skulle være nogen huller som man skal holde ekstra godt øje med.

8. Brug kun PASSIV rekognoscering til at undersøge ucl.dk. Lav en lille rappport med de ting du har fundet ud af og hvordan en angriber evt. kan misbruge oplysningerne. Rapporten skal du IKKE offentliggøres på gitlab, kun på din egen computer, resultater kan du fortælle om til næste undervisning!