# Opgave 34 - MITRE att&ck freamework

1. Lav en hurtig beskrivelse af hvad MITRE att&ck framework er. Inkludér de tre hovedkategorier: Tactics, Techniques and Procedures

- MITRE att&ck er et klassifikationssystem, som bruges til at beskrive og analysere cyperangreb. 
- ATT&CK står for Adversarial Tactics, Techniques and Common Knowledge
- Det er delt op i 3 kategorier, Tactics, techniques og Procedures
    - Tactics (taktikker) repræsenterer de overordnede mål, som angribere stræber efter at opnå under et cyberangreb
    - Techniques (teknikker) repræsenterer specifikke metoder eller måder, som angribere bruger til at opnå deres mål
    - Procedures (Procedyrer) er mere detaljerede beskrivelser af specifikke angrebsmetoder eller angrebskampagner, der er blevet observeret i virkelige scenarier

2. Bonus: Hvorfor TTPs? Hvor kommer det fra?
- Det er med til at lave en opdeling af problemerne, så det er lettere at finde frem til løsningerne
- Det er også med til at skabe bedre forudsætninger for samarbejdet mellem rød og blå hold 
- Oprindeligt stammer TTP fra militæret, hvor det bliver brugt til at beskrive misioner for soldaterne

3. Find 2-3 eksempler i hver kategori. Supplér med en sætning eller to om hvad de dækker over.

- Tactics  
    - Initial Access (Indledende adgang): Denne taktik dækker over metoder, som angribere bruger til at opnå den indledende adgang til et system eller netværk. 
    - Lateral Movement (Sidebevægelse): Denne taktik beskriver metoder, som angribere bruger til at bevæge sig fra en kompromitteret enhed til andre enheder på det samme netværk.
- Techniques 
    - Et eksempel på en teknik inden for Initial Access er "Phishing", hvor angribere sender vildledende e-mails eller beskeder med skadelige vedhæftede filer eller links for at narre brugerne til at afgive følsomme oplysninger eller udføre handlinger, der giver adgang til angriberen.
    - Et eksempel på en teknik inden for Lateral Movement er "Pass the Hash", hvor angribere bruger stjålne hash-værdier til at autentificere sig selv og få adgang til andre systemer uden at kende de faktiske adgangskoder.
- Procedures 
    - Et eksempel på en procedyre til Phising, er "Gold Southfield", som er spammail kampanger, som får adgang til brugernes pc'er
    - Et eksempel på en procedure til "Pass the Hash" er Empire, som kan udføre pass the hash angreb, og få adgang til systemt igennem stjålne hash-værdier