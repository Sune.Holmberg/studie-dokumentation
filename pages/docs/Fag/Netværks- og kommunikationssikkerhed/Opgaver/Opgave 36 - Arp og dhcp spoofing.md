# Opgave 36 

## Læs om ARP spoofing og DHCP spoofing som techniques på Mitre ATT&CK

## Se på mitigations og giv eksempler på hvordan de foreslåede tiltag kan begrænse spoofing.

- ARP spoofing 
    - User training, træn brugerne til at holde øje med hvad det er for nogen certifikat fejl som kan opstå
    - IDS/IPS systemer, til identificere netværks mønstre (eks nmap)
- DHCP spoofing
    - Filter network traffic, ændre på hvilke porte netværkstrafikken fra ukendte steder kommer ind i på netværket
    - IDS/IPS systemer, til identificere netværks mønstre (eks nmap)

## Se på detections og giv eksempler på hvordan de foreslåede tiltag kan detektere spoofing.

- ARP spoofing
    - Monitorering af netværktrafikken, se efter om der sker ting, som ligger uden for den normale netværkstrafik 

- DHCP spoofing 
    - Applikations logs, hold øje med om DHCP indstillingerne bliver ændret 
    - Monitorering af netværktrafikken, se efter om der sker ting, som ligger uden for den normale netværkstrafik 

