# Opgave 40 - Aktive skanningsværktøjer

## Del 1: Interne værtøjer til aktiv skanning

1. Hent [Vulnerable Pentesting Lab Environment: 1](https://www.vulnhub.com/entry/vulnerable-pentesting-lab-environment-1,737/) ([mirror](https://drive.google.com/drive/folders/1gPSNA-2OF9dDJTdhUOcn2N3zPjRBUSWm?usp=sharing))
2. Lav et dokument til at rapportere det i finder undervejs.
3. Åbn maskinen i *vmware workstation* og konfigurer netværk. Maskinen henter sin IP fra DHCP så det vmnet du bruger skal være konfigureret med DHCP i *network manager*. Netværket skal ikke have internetadgang.
4. Sæt en kali maskine på samme vmnet.
5. Tegn et netværksdiagram med hvad du ved på nuværende tidspunkt.
![Netværkdiagram](../../../billeder/Netv%C3%A6rk/Opgave%2040%20-%20f%C3%B8rste%20netv%C3%A6rksdiagram.png)

6. Gør følgende med NMAP fra kali maskinen, du skal have wireshark kørende på samme tid:
    1. scan netværket med ping scan, gem scanning med `-oA <filename>`, hvad finder du og hvordan ser det ud i wireshark? 
        - Kør "nmap -sn 192.168.198.0/24"
    2. Gem en pcapng fil med trafikken fra wireshark
    3. Scan den sårbare maskine, du burde finde mindst 5 åbne porte.
        - Kør "nmap -p1-65535 192.168.198/20"
    4. Hvilke services og versioner er på de 5 porte?
        ![Portscanning](../../../billeder/Netv%C3%A6rk/Opgave%2040%20-%20portscanning.png)
    5. Er der nogle services der er sårbare og hvor kan det undersøges? (hint! cve databaser og exploits) 
        - 
7. Gør følgende med sslscan fra kali maskinen:
    1. prøv ssl scan mod nogen af de services der kører på vple ie. `sslscan <vmip:port>` 
    2. Hvad er output fra sslscan? 
    3. er der services der bruger TLS ?
8. [gobuster](https://www.kali.org/tools/gobuster/#tool-documentation)/[dirbuster](https://www.kali.org/tools/dirbuster/)/[ffuf](https://www.kali.org/tools/ffuf/)/[wfuzz](https://www.kali.org/tools/wfuzz/) er værktøjer til at "fuzze" endpoints på services med henblik på at afdække hvilke endpoints der findes på en service. Afprøv de 4 værktøjer:  
    1. Brug wireshark når du fuzzer og gem relevant (http) trafik fra de forskellige værktøjer som en `.pcapng` fil
    2. Prøv gobuster med:  
    `gobuster dir --url http://<vmip>:1336 -w /usr/share/wordlists/seclists/Discovery/Web-Content/raft-small-words.txt | tee gobuster1336.txt`  
    og se hvad du finder ?
    - Man kan se en masse http pakker, som alle har en "useragent" som er "GOBUSTER" 

    3. prøv dirbuster med:  
    `dirb http://<vmip>:1336 /usr/share/wordlists/seclists/Discovery/Web-Content/raft-small-words.txt | tee dirbuster1336.txt`  
    og se hvad du finder ?
    4. prøv ffuf med:  
    `ffuf -w /usr/share/seclists/Discovery/Web-Content/raft-small-words.txt -u http://<vmip>:1336/FUZZ -c -mc all -r  -fw 24 | tee ffuf1336.txt`  
    og se hvad du finder [ffuf cheatsheet](https://cheatsheet.haax.fr/web-pentest/tools/ffuf/)
    
    5. prøv wfuzz med:  
    `wfuzz -w /usr/share/seclists/Discovery/Web-Content/raft-small-words.txt -u http://<vmip>:1336/FUZZ -c -R 5 --hc 404 | tee wfuzz1336.txt`  
    og se hvad du finder
    6. Sammenlign resultaterne i de 4 filer du har lavet: 
        1. hvilket værktøj fandt mest ?
        2. hvilket værktøj tog længst tid ?
9. Hvilke services synes i er acceptable at have kørende og hvilke er der ikke nogen grund til at eksponere? Nogle specifikke porte?
10. Hvad vil i gøre for at "lukke" de uacceptable services?
11. Er aktiv skanning nok til at finde sårbarheder? Kan der gøres yderligere for at sikre netværk og services?

## Del 2: Eksterne værtøjer til aktiv skanning**

1. Shodan
    1. Lav en konto på shodan, den der er gratis (tip! hold øje på black friday hvis du vil have rabat til at blive member)
    2. Læs om hvad shodan kan ifht. netværks sikkerhed: [https://help.shodan.io/the-basics/what-is-shodan](https://help.shodan.io/the-basics/what-is-shodan)
    3. Læs om [shodan søgninger](https://help.shodan.io/the-basics/search-query-fundamentals)
    4. Lav følgende søgninger
        - `airplay port:5353`
        - `has_screenshot:true`
        - `http.title:"Nordex Control" "Windows 2000 5.0 x86" "Jetty/3.1 (JSP 1.1; Servlet 2.2; java 1.6.0_14)"`
    5. Udforsk selv yderligere muligheder, kig f.eks i de søgninger andre har lavet:  
    [https://www.shodan.io/explore/recent](https://www.shodan.io/explore/recent)
2. Whois
    1. Hvad viser den grønne knap med "find problems" på [whois](https://mxtoolbox.com/SuperTool.aspx?action=whois%3aucl.dk&run=toolpage) ?
3. ssllabs
    1. Hvad kan i finde ud af på [ssllabs](https://www.ssllabs.com/ssltest/) 
    2. [test ucl.dk](https://www.ssllabs.com/ssltest/analyze.html?d=ucl.dk&latest) og se om der er problemer

## Links

- Inspiration [ippsec interface](https://youtu.be/yM914q6zS-U)
- Introduction to [shodan monitor](https://youtu.be/T-9UvZ-l-tE)
