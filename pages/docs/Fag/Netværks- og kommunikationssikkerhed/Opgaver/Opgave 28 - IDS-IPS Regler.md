# Opgave 28 - IDS/IPS Regler

1. Læs om regler i [Suricata dokumentationen](https://suricata.readthedocs.io/en/suricata-6.0.11/rules) og dan dig et overblik over mulighederne.

2. Lav en ny mappe på den vm hvor du har installeret Suricata
- "sudo mkdir IDS-IPS"

3. På suricata maskinen hent pcap filen fra https://www.malware-traffic-analysis.net/2018/07/15/2018-07-15-traffic-analysis-exercise.pcap.zip (brug wget, zip password er infected)

- For at hente filen, skal man bruge "sudo wget https://www.malware-traffic-analysis.net/2018/07/15/2018-07-15-traffic-analysis-exercise.pcap.zip" 

- For at unzippe filen, skal man først hente "sudo apt install unzip", og derefter køre "sudo unzip 2018-07-15-traffic-analysis-exercise.pcap.zip" 

4. Skriv en regel i "/etc/suricata/rules/local.rules" der laver en alert på udp trafik fra "10.0.0.201 port 63448" til "5.138.53.160 port 41230"
Dokumenter hvor mange alerts du får og hvad output fra de relevante alerts er i fast.log ?

![Reglen](../../../billeder/Netv%C3%A6rk/Opgave%2028%20-%20ny%20regl%20til%20udp.png)

- Kør "sudo suricata -v -r 2018-07-15-traffic-analysis-exercise.pcap.zip" 

![alerts](../../../billeder/Netv%C3%A6rk/opgave%2028%20-%20vanilla%20fastlog%20alerts%20.png)

5. Skriv en regel der alerter på http trafik fra alle ip og porte til alle ip og porte hvor http content type er image/jpeg Dokumenter hvor mange alerts du får og hvad output fra de relevante alerts er i fast.log ?

- Reglen "alert http any any -> any any (msg:"image detected";flow:to_client; http.content_type; content:"image/jpeg"; sid:5000002;)"

![Alerts](../../../billeder/Netv%C3%A6rk/opgave%2028%20-%20image%20detection%20fastlog.png)

6. Skriv en regel der alerter på http trafik fra alle ip og porte til alle ip og porte hvor http content type er application/x-bittorrent

- Reglen "alert http any any -> any any (flow:to_client; http.content_type; content:"application/x-bittorrent"; sid:5000003;)"

![application/x-bittorrent](../../../billeder/Netv%C3%A6rk/Opgave%2028%20-%20app-xbittorrent%20alerts.png)

7. Skriv en regel der alerter på http trafik fra alle ip og porte til alle ip og porte hvor ordet Betty_Boop indgår.
Dokumenter hvor mange alerts du ser, samt hvilke ip adresser og porte der er involveret i kommunikationen ?

- Reglen "alert http any any -> any any (msg:"Betty_Boop detected"; file_data; content:"Betty_Boop";)

![Betty_Boop](../../../billeder/Netv%C3%A6rk/Opgave%2028%20-%20Betty_Boop%20alerts.png)

