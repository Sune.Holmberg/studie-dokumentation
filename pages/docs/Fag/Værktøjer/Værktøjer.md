# Forskellige værktøjer som kan bruges

## Monitorering
### Wireshark
### Ntopng
### Netbox

## Portainer.io

## Draw.io
- Kan bruges til at tegne diagrammer (Fysiske og logiske netværksdiagrammer)

## Vmware - Virtuelle maskiner
### Linux
#### Kali
#### Debian
#### Ubuntu

## Træningsværktøjer
### TryHackMe

## Keywords
### TLS
### SSL
### SSH
### VAN 
### LAN