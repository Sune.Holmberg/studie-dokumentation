# Noter

## Kali linux installation
Installer kali linux i VMware

1. Download VMWare workstation -> https://www.vmware.com/products/workstation-pro/workstation-pro-evaluation.html

2. Download kali linux -> https://www.kali.org/get-kali/#kali-virtual-machines

3. Følg guiden -> https://www.kali.org/docs/virtualization/import-premade-vmware/

4. Start en VM op, og åben kali linux (brugernavn og password er kali/kali)


## VPN til TryHackMe
Følg guiden -> https://tryhackme.com/room/tutorial

Download openVpn fra -> https://tryhackme.com/access?o=vpn

Hvis EU-Regular-3 ikke virker, så brug EU-Regular-1, og tryk på donwload knappen.

Når man skal vpn til thm, skal man først starte maskinen op den opgave man er i gang med. Når maskinen kører, kan man kopiere den ip-adresse som maskinen giver. I terminalen på kali linux, skal man køre kommandoen "ssh tryhackme@"ip-adressen". Passwordet er "tryhackme". 

Test