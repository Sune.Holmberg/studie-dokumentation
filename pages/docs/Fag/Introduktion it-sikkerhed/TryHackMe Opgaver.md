# TryHackMe opgaver

## Linux fundamentals part 1
I denne part, lærer man omkring navigationen rundt og finde frem til filer som er gemt på computerne igennem kommandolinjen. 

## Linux fundamentals part 2
I denne part, lærer man omkring navigation ved hjælp af "cd" "ls", samt at kunne skifte imellem 2 brugere ved hjælp af "su brugernavnet". I opgaverne lærer man også at se hvad for en type file er ved hjælp af "file" og at se content ved hjælp af "cat" kommandoen. 

## Linux fundamentals part 3
I part 3 kigger man på mere avanceret kommandoer. Bla. "Systemctl" som man kan bruge til at state processer, når man booter sit system op. I denne part lærer man også at kunne bruge log filer til at finde frem til hvilke ip-adresser som brugeren har besøgt. Det gør man igennem gemte log filer som ligger i "/var/log/apache2". 

## What is networking?
I opgaven får man en beskrivelse af hvad et netværk er. Man kommer også til at prøve "ping" kommandoen ad, hvor man kan test om man har en forbindelse til den ip adresse som man pinger. Man får også en introduktion til MAC-adresser, om man kan se at man kan tilkoble andres internet, hvis man får adgang til mac-adressen. 