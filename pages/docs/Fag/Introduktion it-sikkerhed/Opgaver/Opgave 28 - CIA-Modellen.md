# Opgave 28 - CIA Modellen
1. Læs om CIA modellen i bogen "IT-Sikkerhed i praksis, en introduktion" kapitel 2.
2. Vælg et af følgende scenarier som i vil vurdere i forhold til CIA: 
- lægeklinik (virksomhed)
3. Vurder, prioitér scenariet i forhold til CIA modellen. Noter og begrund jeres valg og overvejelser.
 -  C = Confidentiality I = Integrity  A = Availability

    - C = Folks lægejunaler indeholder mange person følsomme iformationer, noter som ligger på bordet

    - I = skal være sikker på der står det rigtige i mappen så lægen ikke begynder at oprer i det forkerte ben.

    - A = skal være tilgengelig for medarbejderen hele tiden

4. Hvilke(n) type hacker angreb vil være mest aktuelt at tage forholdsregler imod? (DDos, ransomware, etc.)
- Ddos
- Social engenering
- Ransomware
- Man in the middel 
- Indsider trussel  

5. Hvilke forholdsregler kan i tage for at opretholde CIA i jeres scenarie (kryptering, adgangskontrol, hashing, logging etc.)
- ISO 27002:2022
    - Organisatoriske foranstaltninger
        - Sikkerheds politiker
        - Clean desk (Fjerner fortrolige ting fra skrivebordet)
        - Låser computeren når du forlader den
        - Kryptering (HTTPS)
        - Ikke tilgå offentlige netværker fra arbejdscomputeren

    - Tekniske foranstaltninger
        - Login 
        - Logging 
        - Netværks setværterking

    - Fysiskes foranstaltninger
        - Adgangs kort
        - Camara
        - Speed gate

    - Person relaterforanstalinger
        - Bagrunds tjek

6. Hver gruppe fremlægger det de er kommet frem til og alle giver feedback.
