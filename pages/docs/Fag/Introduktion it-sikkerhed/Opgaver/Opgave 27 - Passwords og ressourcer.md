# Opgave 27 - Passwords og ressourcer

1. Undersøg hvor gammel den nuværende password politik hos UCL er (kig efter gamle microsoft password anbefalinger)
    - Hvis man ucl følger centeret for cypersikkerhed, så er den fra 24. Januar 2020

2. Debatter om hvilke konsekvenser IT politikker har for miljøet og virksomheder ressource forbrug.
    - Der skal bruges flere ressourcer på at skifte adgangskoder 
    - Større change for at underviserne glemmer deres nuværende adgangskode, fordi der er flere forskellige de skal huske på
    
3. Snak om andre områder indenfor IT som i kan påvirke i forhold til miljør og ressourcer.
    - Nedkøling af serverrum/genbrugsvarme  
    - Mindre budget til andre ting

4. Undersøg hvad anbefalingerne til password politik er i 2022. Tag udgangspunkt i https://learn.microsoft.com/en-us/microsoft-365/admin/misc/password-policy-recommendations og find selv flere kilder.
    - Minimum 14 tegn 
    - Blanding af store og små tegn 
    - Skift adgangskode 1 gang i 3. måneden
    - Brug 2 factor authentication

5. Nævner ISO27000 eller andre standarder noget om password politik ?
    - Minumum 8 tegn og maks 64
    - Mulighed for at bruge specialtegn 
    - Ingen dårlige password

6. Skriv et forslag til en opdateret miljø og ressource besparende password politik (i må gerne tænke alternativt i f.eks yubikey eller MFA) og giv jeres bedste bud på hvad det kan spare miljøet og UCL for i ressourcer.
    - Ingen krav om skifen af password 
    - Ingen dårlige password 
    - Tvunget specialtegn 