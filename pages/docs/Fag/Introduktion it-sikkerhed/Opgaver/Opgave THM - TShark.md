# TryHackMe - TShark

1. Join rummet -> ![Rummet](../../../billeder/Intro-its/THM-tshark%20join%20room.png) og download TShark med kommandoen "sudo apt install tshark"

2. Download pcap filen -> ![Rummet](../../../billeder/Intro-its/THM-tshark-first-pcap-file.png)

- Brug "tshark -r dns.cap" til at finde ud af hvor mange pakker der er i filen -> ![Packets](../../../billeder/Intro-its/thm-tshark-dns.pcap-antal-packets.png)

- Brug "tshark -r filename.cap -Y “dns.qry.type ==1”" til at finde ud af hvor mange A-records som bliver gemt -> ![Packets](../../../billeder/Intro-its/thm-tshark-number-of-a-records.png)

- På billedet over kan man også se hvilken A-Record som bliver vist flest gange

3. Download pcap filen 

- Brug "tshark -r dnsexfile.pcap" til at se antal pakker 

- Brug "tshark -r filename.pcap “dns.flags.response == 0”" til at se DNS quries -> ![Packets](../../../billeder/Intro-its/thm-tshark-antal-dns-quries.png)

- Undersøg pakken og find id'et i hex -> ![Packets](../../../billeder/Intro-its/thm-tshark.dns-it-in-hex.png)

- Følg udp strømmen, og samlig de ting som er forskellige ![Packets](../../../billeder/Intro-its/THM-tshark-dnsexfile-diferences.png)

- Brug https://gchq.github.io/CyberChef/ til at oversætte koden fra 32bit for at finde flaget