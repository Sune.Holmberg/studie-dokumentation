# Opgave 8 - Python recap
## Del 1

    #Definere en funktion der tager to parametre og returere deres sum
    def add(num1, num2):
        return num1 + num2

    def sub(num1, num2):
        return num1 - num2

    def div(num1, num2):
        return num1 / num2

    def mult(num1, num2):
        return num1 * num2

    #Printer velkomsbesked til brugeren
    print('\nWelcome to the simple calculator (write q to quit)')

    #variabel for resultatet af handlingen og den valgte handling 
    result = 0.0
    chosen_operation = ''

    #Starter et uendeligt loop der beder brugeren om at indtaste to tal og en handling (add, sub, div, mult) indtil de vælger at afslutte med q.
    while True:
        print('enter the first number')
        number1 = input('> ')
        if number1 == 'q':
            print('goodbye...')
            break
        print('enter the second number')
        number2 = input('> ')
        if number2 == 'q':
            print('goodbye...')
            break
        print('would like to: 1. add 2. subtract 3. divide or 4. multiply the numbers?')
        operation = input('> ')
        if operation == 'q':
            print('goodbye...')
            break

        #Konverter den inputede string til float og integer
        try:
            number1 = float(number1)
            number2 = float(number2)
            operation = int(operation)

            #Kalder på den definerede funktion til at lave udregningen baseret på brugeren valg
            #Definere 'chosen_operation' til en string der bruges til at printe beskeden til brugeren
            if operation == 1:
                result = add(number1, number2)
                chosen_operation = ' added with '
            elif operation == 2:
                result = sub(number1, number2)
                chosen_operation = ' subtracted from '
            elif operation == 3:
                result = div(number1, number2)
                chosen_operation = ' divided with '
            elif operation == 4:
                result = mult(number1, number2)
                chosen_operation = ' multiplied with '

            #Printer resultatet af udregningen og genstarter loopet 
            print(str(number1) + chosen_operation + str(number2) + ' = ' + str(result))
            print('restarting....')

        "Hvis konverteringen til float/integer fejler, så printes en fejlbesked og loopet genstartes
        except ValueError:
            print('only numbers and "q" is accepted as input, please try again')

        #Hvis brugeren prøver at div med 0, så printes en fejlbesked og loopet genstartes 
        except ZeroDivisionError:
            print('cannot divide by zero, please try again')

## Del 2
    import logging

    logging.basicConfig(filename="Opg8Log.log", level=logging.INFO)
    logger=logging.getLogger() 
    logger.setLevel(logging.DEBUG)

    def operate(num1, num2, operation):
        if operation == 1:
            return num1 + num2, ' added with '
        elif operation == 2:
            return num1 - num2, ' subtracted from '
        elif operation == 3:
            return num1 / num2, ' divided with '
        elif operation == 4:
            return num1 * num2, ' multiplied with '
        else:
            return None, ''

    print('\nWelcome to the simple calculator (write q to quit)')

    result = 0.0
    chosen_operation = ''

    while True:
        print('enter the first number')
        number1 = input('> ')
    
        if number1 == 'q':
            print('goodbye...')
            break

        print('enter the second number')
        number2 = input('> ')
        if number2 == 'q':
            print('goodbye...')
            break

        print('would like to: 1. add 2. subtract 3. divide or 4. multiply the numbers?')
        operation = input('> ')
        if operation == 'q':
            print('goodbye...')
            break

        try:
            number1 = float(number1)
            number2 = float(number2)
            operation = int(operation)

            result, chosen_operation = operate(number1, number2, operation)

            print(str(number1) + chosen_operation + str(number2) + ' = ' + str(result))
            logging.info(number1, chosen_operation, number2, result)
            logging.warning('Restarting')
            print('restarting....')

        except ValueError:
            logging.error('only numbers and "q" is accepted as input')
            print('only numbers and "q" is accepted as input, please try again')

        except ZeroDivisionError:
            logging.error('cannot divide by zero')
            print('cannot divide by zero, please try again')