# Opgave 10 - Bash scripting
For at oprette en fil til scriptet:
-nano "filnavn".sh

Øverst i filen skal der står:
- !# "stien til bash"
Feks
- !# /bin/bash

For at få scriptet til at udfører en kommando og udskrive resultatet af den:
- echo $("kommando")

Eksempler:
- echo $(ifconfig)

- echo $(whoami)

Man kan også bruge kommandoen "alias":

Med alias kan man lave en kortere kommando som udfører ens lange kommando

Fx:

alias ipadress="echo $(ifconfig | grep broadcast | awk '{print $2}')"

-> kommandoen udskriver ens ip-adresse, men uden alle de andre oplysninger som også kommer med den almindelige ifconfig.

Man kan også få kommandoer til at kører i baggrunden:

Det gøres ved i scripet at tilføje "&" efter kommandoen/kommandoerne som man vil kører i baggrunden.

Se evt. følgende video for yderligere uddybning af de fleste af tingene:

https://www.youtube.com/watch?v=PPQ8m8xQAs8