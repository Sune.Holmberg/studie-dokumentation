# Opgave 9  - OSINT med github

## Information
Kildekoden til en applikation i en offentligt repository på Github, udstiller uhensigtsmæssigt et 
stykke information for meget. Kan du finde den information?

Link til repo: [https://github.com/mesn1985/DotNetApplicationWithToMuchInformation](https://github.com/mesn1985/DotNetApplicationWithToMuchInformation)

## Links

## Løsning

- Løsningen kan findes under "apsettings.json", hvor der ligger en api i plain text 

![Apsettings.json](../../../billeder/Web-sikkerhed/Opgave%209.PNG)