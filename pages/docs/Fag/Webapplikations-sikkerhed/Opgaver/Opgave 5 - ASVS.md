# Opgave 5  - ASVS

## Information
I denne øvelse skal du besvare en række spørgsmål om [OWASP Application Security Verification Standard](https://github.com/OWASP/ASVS/tree/v4.0.3#latest-stable-version---403).
(Du kan finde den seneste udgave ved at klikke på linket)

## Instruktioner
1. I hvilket afsnit kan man finde et overblik over hvad man bør sikre sig ift. input validering?
2. Hvad beskrives der i afsnit _V1 Architecture, Design and threat modelling_
3. I nogen afsnit referes der til _NIST_ , hvad er NIST?

  
## Links

1. I PDF'en og afsnit v1.5

2. I afsnittet "V1 Architecture, Design and Threat Modelling" beskrives strukturen af hvordan man påbegynder tingene, og regler/best practies man burde følge

3. NIST (National Institute of Standards and Technology):
NIST er en amerikansk føderal myndighed under Department of Commerce, der er ansvarlig for at udvikle og fremme standarder og retningslinjer inden for en bred vifte af teknologiske områder, herunder informationssikkerhed og cybersikkerhed. NIST's retningslinjer og standarder er ofte referencer inden for sikkerhedsområdet og anvendes både i USA og internationalt som en kilde til bedste praksis og retningslinjer inden for informationssikkerhed.