# Opgave 1 (gruppe øvelse) -  Vidensdeling fra forberedelsen til idag.

## Information
Formålet med denne øvelse er at gruppen laver en opsamling på den viden hvert medlem 
har opnået gennem forberedelsen til idag.  
Hvis man blot læser, arbejder eller ser noget en enkelt gang er det ofte ikke nok til 
at opnå en forståelse for emnet. Hvis man taler om emnet med ligesindet efter man har læst 
noget, vil der typisk opnåes en endnu større forståelse for emnet. Herudover træner det brugen
af fag termer og terminologier.

## Instruktioner
Gruppen skal  sammen afklarer følgende spørgsmål:
- Hvad er et HTTP request?  
- hvad er en HTTP metode?  
- Hvor kan man finde en oversigt over HTTP metoders semantiske betydning?  
- Hvad er et REST api?  
- Hvad betyder at et REST api er stateless?  

## Links

## Løsninger

- HTTP-request:

Besked sendt fra en klient til en webserver.
Anmoder om en bestemt handling, f.eks. at hente data eller udføre en handling.

- HTTP-metode:

Handling eller operation i en HTTP-anmodning.
De mest almindelige metoder inkluderer GET, POST, PUT og DELETE.

- Oversigt over HTTP-metoders semantiske betydning:

Findes i officielle dokumentationer som RFC 2616 (ældre) eller RFC 7540 (nyere).
Kan også findes i bøger om webudvikling og online kilder som MDN eller W3Schools.

- REST API (Representational State Transfer API):

En arkitektonisk stil for webtjenester.
Bruger HTTP-metoder til at udføre operationer på ressourcer.
Benytter URI'er (Uniform Resource Identifiers) til at identificere ressourcer.
Ofte bruges JSON eller XML til at overføre data.

- Stateless i et REST API:

Betyder, at hver HTTP-anmodning fra klienten til serveren er uafhængig og indeholder alle nødvendige oplysninger.
Serveren gemmer ikke information om klientens tidligere anmodninger.
Dette gør det lettere at skalere og opretholde API'en, da der ikke er nogen tilstand (state) gemt mellem anmodninger.
