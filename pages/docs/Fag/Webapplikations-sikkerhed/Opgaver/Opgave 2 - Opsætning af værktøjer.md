# Opgave 2  -  Opsætning af værktøjer

## Information
I denne øvelse skal du installer de værktøjer vi indledningsvist skal bruge i faget.
Følgende skal installeres:  
- Burp suite.  Et værktøj til analyse af web applikationer
- Postman. Et værktøj til at lave HTTP requests
- Docker. En appliaktion der kan eksekver _Containerized_ applikationer.

Til sidst skal du eksekver de applikationer som vi indledningsvist skal anvende
i faget.

Det formodes at i allerede har lavet en opsætning med Kali Linux.

## Instruktioner
1. Installer Burp suite community edition: [Install burp suite](https://portswigger.net/burp/documentation/desktop/getting-started/download-and-install) 
_Er allerede installeret i Kali Linux, men er også praktisk at havde i windows_
2. Installer Postman [Install postman](https://www.postman.com/downloads/)
_I behøver ikke at oprette en bruger for at bruge postman_
3. Installer docker desktop [Install Docker](https://www.docker.com/products/docker-desktop/)
  
Efter værktøjer er installeret, skal _Hacker miljøet_ sættes op, gør følgende:  
1. hent Filen _docker-compose.yml_ der kan findes under ressourcer på its learning planer til dags dato.  
2. Læg filen i en folder hvor du kan finde den igen på et senere tidspunkt.  
3. navigere til folderen med powershell.  
4. i powershell, eksekver kommandoen `docker-compose -f docker-compose.yml --compatibility up -d`, og vent.  
5. Når miljøet er færdig med at blive op, kan du verificere at det virker ved at gå ind på `http://localhost:8888` & `http://localhost:3000/`  
  
## Links

