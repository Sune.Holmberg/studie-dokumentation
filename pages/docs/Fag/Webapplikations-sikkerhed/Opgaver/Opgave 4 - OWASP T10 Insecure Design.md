# Opgave 4  - OWASP top 10 Insecure design

## Information
I denne øvelse skal du arbejde med [OWASP top 10 number 4 - Insecure design](https://owasp.org/Top10/A04_2021-Insecure_Design/).
Inde på siden(og dertil hørende links), skal du forsøge at finde svar på de 3 nedstående spørgsmål.

## Instruktioner
1. Hvad bliver overordnet beskrevet i afsnittet _Requirements and Resource management_, og kender du en tilgang som dækker dele af det beskrevet?
2. Hvad menes der med Secure design?
3. I afsnittet _Secure design_ bliver der nævnt en type modellering som bør indgår i en udviklings process. Hvilken type modellering er der her tale om?

  
## Links

## Løsning 

1. Requirements and Resource Management:

Designfasen skal spille overens med bla. CIA-modellen

2. Secure Design:

Man tænker sikkerheden ind i det undervejs.

3. Type af modellering i Secure Design:

- Trusselsmodellering

Trusselsmodellering er en proces, hvor udviklingsteamet identificerer og analyserer potentielle trusler og sårbarheder i systemet. Dette gøres normalt ved at oprette en trusselsmodel, der beskriver de forskellige aktiver i systemet, potentielle trusler mod dem og de sikkerhedsforanstaltninger, der skal træffes for at beskytte dem. Trusselsmodellering hjælper med at informere beslutninger om sikkerhedsdesign og prioritering af sikkerhedsforanstaltninger.