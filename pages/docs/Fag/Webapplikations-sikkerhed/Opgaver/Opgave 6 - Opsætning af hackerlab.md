# Opgave 6  - Opsætning af hackerlab 

## Information
I denne øvelse skal der (igen)laves en opsætning med sårbar applikationer.
Applikationerne erstatter opsætningen fra forrige uge.

Docker-compose filen samt dokumentation kan findes her: [https://github.com/mesn1985/HackerLab](https://github.com/mesn1985/HackerLab)

_Fjern evt. docker container og images fra forrige lektion_

## Instruktioner
1. Klone [repositoriet](https://github.com/mesn1985/HackerLab) med git.
2. Fjern alle evt. tidligere opsætninger fra docker. (F.eks. Docker-compose down, eller via docker gui'en)
3. Følg instruktionerne for opsætningen i repositoriets `README.md` file.
4. Efter opsætningen test at du kan tilgå applikationerne `Damn vulnerable web application`,`Juice shop` og `crAPI`.  
_Portene hver applikation bruger kan du se i readme filen_

## Links
[Hackerlab](https://github.com/mesn1985/HackerLab)
