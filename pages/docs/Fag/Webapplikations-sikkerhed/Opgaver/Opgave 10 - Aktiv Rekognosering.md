# Opgave 10  - Aktiv Rekognoscering

## Information
I disse øvelser skal du arbejde med aktiv Rekognoscering.
Vil anvende den teknik der kaldes _path traversal_ eller _directory attack_.
Tilgangen er at man anvender en liste kaldet en _Wordlist_, som indeholder flere forskellige
ord. Hver af disse ord prøver man så af mod en web applikation, og ser hvilket http status kode 
den retunerer. Man anvender værktøjer til at automatisk udføre afprøvning af hvert ord. I disse
øvelser vil i skulle anvende Gobuster og OWasp ZAP.

Der er en del læring i øvelserne, så bevæg dig langsomt frem. Du kommer til at skulle læse op på
værktøjerne mens du udføre øvelserne.

**Husk at gemme de Juiceshop og crAPI wordlister du laver løbende gennem øvelserne**

## Instruktioner
1.  Læs afsnittet [prerequisites](https://github.com/mesn1985/HackerLab/blob/main/crAPI/3_Active_reconnaissance.md#prerequisites) og installer de nødvendige værktøjer.
2.  Udfør [Øvelse 1](https://github.com/mesn1985/HackerLab/blob/main/crAPI/3_Active_reconnaissance.md#1-nmap-all-ports)
3.  Udfør [Øvelse 2](https://github.com/mesn1985/HackerLab/blob/main/crAPI/3_Active_reconnaissance.md#2-nmap-service-scan)
4.  Udfør [Øvelse 3](https://github.com/mesn1985/HackerLab/blob/main/crAPI/3_Active_reconnaissance.md#3-enumerating-crapi-with-gobuster-and-the-wordlist-commontxt)
5.  Udfør [Øvelse 4](https://github.com/mesn1985/HackerLab/blob/main/crAPI/3_Active_reconnaissance.md#4-enumerating-crapi-with-gobuster-and-the-wordlist-quickhitstxt)
6.  Udfør [Øvelse 5](https://github.com/mesn1985/HackerLab/blob/main/crAPI/3_Active_reconnaissance.md#5-enumerating-crapi-with-zap)  
_Zap kan  godt virke en smule overvældende når man skal lære at bruge det for første gang. Men læs den dokumentation som er linket i opgaven, og hvis du går i stå, så spørg underviseren_
7. Afprøv de wordlister du selv har udarbejdet løbende.

## Links
