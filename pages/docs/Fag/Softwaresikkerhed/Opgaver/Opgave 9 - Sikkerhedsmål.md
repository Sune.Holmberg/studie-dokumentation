# Øvelse 9 - (Gruppe øvelse) udarbejdelse af sikkerheds mål fra borger.dk

## Information
Formålet med denne øvelse er træne overgangen fra system mål til sikkerheds mål, og
udarbejdelse af sikkerhed mål som misbrugs tilfælde.
Alle sikkerheds mål udledes direkte fra system målene. Her er det vigtigt at huske
at det stadig ikke er den tekniske trussel der fokuseres på, men i stedet hvordan forretningen
kan blive skadet hvis systemet bliver misbrugt.


## Instruktioner  
1. Brainstorm for mulige misbrugstilfælde ud fra brugstilfælde diagrammet fra forrige øvelse.
**Påtegn løbende misbrugstilfældene på diagrammet, se eksemplet fra dagens forberedelse**
2. Udarbejde 2 eller flere misbrugstilfælde **Se eksemplet i forberedelse til idag**

Titel: Forfalsk hjemmeside 					Aktør: Ekstern IT kriminel

En ældre borger skal tilgå sin pension. En ekstern IT kriminel har opsnappet dette, og har derfor forfalsket en hjemmeside, som ligner til dels den original / rigtige side som borgeren skal bruge. Aktøren snyder derfor borgerens tillid ved at replikere dets udseende. Inde fra den falske hjemmeside, kan den IT kriminelle overvåge, og samle borgerens pensions information, såsom balancen, udtræk mm. uden at virke suspekt.