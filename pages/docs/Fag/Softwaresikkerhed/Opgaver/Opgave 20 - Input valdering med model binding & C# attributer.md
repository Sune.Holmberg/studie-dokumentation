# Øvelse 20 - Input validering med model binding & C# attributer

## Information
Formålet med denne øvelse er at du stifter bekendtskab med model binding og validering med denne tilgang.
Modelbinding er når rammeværktøjet (ASP .Net core) automatisk mapper modtaget data, til en bestemt data type.

I øvelsen skal projektet `ValidationModelBinding` bruges.
Hvis du inspicerer koden i controller klassen `NameController.cs` vil du bemærke at der ikke er
implementeret nogen validering metode. I stedet har metoden `ValidateName` et stykke kode som 
eksekverer validering (`!ModelState.IsValid`). Denne kode Evaluerer om objektet der bliver givet
som argument, overholder nogle opsatte regler for lige netop dette objekt(Det vender vi tilbage til).
Metoden ValidateName er annotreret med `[HttpPost]` hvilket betyder at denne medtode er beregnet til at  
modtage data der er sendt med et post request (Fra post formen som blev introduceret i [Øvelse 19](19_Input_validering_med_post_form.md)).

Hvis du kigger i projekt folderens `Models -> PersonDTO.cs` vil du kunne se implementeringen af den klasse, som  `ValidateName` metoden modtager som type af argument.
Det er denne implementering som `!ModelState.IsValid` anvender til at kontroller om alle reglerne for dataen er overholdt. Dette gøre ved at kigge på såkaldte 
`attributer`, et eksempel på en attribute er `[Notnull]` som `Firstname` og `Lastname` er annoteret med. `[Notnull]` opstiller en regel om at denne data skal være sat,
ellers fejler valideringen.

I denne øvelse skal du implementerer input valideringen ved at tilføje flere attributer til `Firstname` og `Lastname`.
Du kan finde hjælp med følgende:  
[Regex Attribute](https://learn.microsoft.com/en-us/dotnet/api/system.componentmodel.dataannotations.regularexpressionattribute?view=net-7.0)   
[String minimum length](https://learn.microsoft.com/en-us/dotnet/api/system.componentmodel.dataannotations.stringlengthattribute.minimumlength?view=net-7.0)  
[String maximum length](https://learn.microsoft.com/en-us/dotnet/api/system.componentmodel.dataannotations.stringlengthattribute.maximumlength?view=net-7.0)   
  
Du skal blot placerer de nye attributer oven eller under `Not null` attributterne for `Firstname` og `Lastname`.
Det er ** kun ** klassen `PersonDTO` du skal ændre i.

## Instruktioner
_Alle validering udføres ved at anvende attributter_  
1. Valider at navnet i argumentet `nameInput` ikke er længere 20 karakterer  
2. Valider at navnet i argumentet ikke er kortere end 3 karakter  
3. Valider at navnet kun indeholder alfabetiske karakter, med Regex (ikke andre tilgange).  
4. Sikker dig at validering virker ved at eksekver applikationen, og afprøv med et par forskellige inputs.  

![Løsning](../../../billeder/Software-sikkerhed/Opgave%2020.png)
