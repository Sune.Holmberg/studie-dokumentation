# Opgave 3 - Hvad er sikkerhed?

- Hvad er sikkerhed?
    - Sikkerhed refererer til beskyttelsen af aktiver, data, systemer, og personer mod en række trusler og fare

- Hvornår er der brug for sikkerhed?
    - Sikkerhed er nødvendig i en bred vifte af situationer og områder i vores daglige liv