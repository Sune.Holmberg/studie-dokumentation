# Øvelse 34 - Håndtering af exception i ASP .Net Core Applikationer

## Information
Formålet med denne øvelse, er at vise den grundlæggende tilgang til overordnet håndtering af exceptions
i applikationer.

Øvelsen viser blot den overodnet og generiske tilgang til at sikre, at applikationen ikke uhensigtmæssigt
eksponerer en stack trace. Men ikke hvordan man gennemgående skal håndterer exceptions, da dette emne 
vil blive for stort ift. emnets størrelse i faget. Såfremt man ønsker at lære mere om håndtering af 
uventede fejl (Exceptions) kan man referrer til programmeringssproget og frameworkets dokumentation,
F.eks. til MS docs ASP .Net core [Handling exceptions in ASP .Net Core](https://learn.microsoft.com/en-us/aspnet/core/fundamentals/error-handling?view=aspnetcore-7.0).

Tilgangen  der skal arbejdes med referes ofte som _Global exception handling_. Dette betyder at alle exceptions som ikke bliver grebet(med F.eks. en Try/catch statement)
altid vil blive håndteret af en generisk exception handler, der typisk returner en generisk fejl tilbage til slut brugeren (F.eks. HTTP status kode 500 i web applikationer).

I [WebGoat](https://github.com/tobyash86/WebGoat.NET) applikationen er der implementeret en global håndtering af exceptions. Dette kan ses 
i metoden _Configure_ i _Startup_ klassen. i nedstående udsnit er dette vist.

```CSharp
if (env.IsDevelopment())
    {
        app.UseDeveloperExceptionPage();
    }
else
    {
        app.UseExceptionHandler($"/{StatusCodeController.NAME}?code=500");
    }
```
I forrige var det muligt at fremprovokerer en stacktrace fordi applikationen eksekverer i udviklings miljø. Dette er belejligt når udvikler
skal ændre på software, for så at kunne se de konkrette fejl meddelelser. Men i et produktions miljø er dette ikke hensigtsmæssigt, da det som vist
i forrige opgave, vidergiver information til potentielle fjendtlige aktører.

*Det er vigtig at understrege, at global fejl håndtering er en sidste "nød bremse" i tilfælde af uforudset fejl, der ikke bliver håndteret af applikationen*.
Kan en angriber fremprovokere en status 500 meddelelse, vil dette stadig indikerer overfor angriberen at der er blevet fremprovokeret en fejl. Angriberen
kan dog ikke se hvilken fejl, eller udlæse informationener fra stacktrace.

For yderlige informationer om fejlhåndtering kan du læse MS DOCS [Handling exceptions in ASP .Net Core](https://learn.microsoft.com/en-us/aspnet/core/fundamentals/error-handling?view=aspnetcore-7.0), eller (gen)læse kapitel 9 i bogen _Secure by Design_.


## Instruktioner
1. I klassen _Startup_ skal du udkommenterer koden for global håndtering af exceptions(tidligere vist i afsnittet _Information_), og i stedet indsætte `app.UseExceptionHandler($"/{StatusCodeController.NAME}?code=500");`
2. Eksekver applikationen, og frem provoker en fejl med `'` eller `;` (samme fremgang måde som i forrige øvelses).
3. Noter dig hvilken fejl der opstår? Giver den fejl mening? Og er den fejl valgt fremfor F.eks. Status kode 500?

Fejl meddelsen er ikke korrekt, og ikke særligt sigende. Men her er det vigtig og huske, at den globale håndtering af exceptions kun er en nødløsning til de 
forhåbentlige få tilfælde af exceptions der ikke bliver grebet i en applikation. Altså exceptions bør som udgangspunkt være håndteret i koden, og opstår kun
i de tilfælde hvor noget uventet går galt. Hvis en domæne primitiv F.eks. kaster en Exceptions, indikerer det at input valideringen ikke har fanget noget dårligt data,
der er blevet sendt over fra klienten. Og ift. til almindelige bruger (der ikke har ondsindet intentioner), bør klient side valideringen(F.eks. i browseren) allerede havde forhindret brugeren
i at sende den dårlige data.  
  *Husk klient side validering er ikke en sikkerheds mæssig foranstaltning!, blot en indikator til brugeren om at data ikke overholder formatet*.  
Man bør tænke _defense in depth_ når man tænker håndtering af exceptions. Og global exceptions håndtering er det sidste lag i denne kontekst. Dette 
forhindre at en stack trace ikke uhensigtsmæssigt bliver eksponeret til uvedkommende. Men angriberen kan dog stadig se at inputtet udløser en fejl.  
  
I de næste trin af øvelsen skal du helt fjerne den globale fejl håndtering.

1. I i metoden `Configure`, i klassen _Startup_, skal du udkommenterer `app.UseExceptionHandler($"/{StatusCodeController.NAME}?code=500");`
2. Eksekver applikationen, og frem provoker en fejl med `'` eller `;`
3. Noter dig hvilken fejl der opstår? Hvilken HTTP status kode bliver der retuneret? 

ASP .Net Core understøtter princippet om _Secure by default_. Dette betyder at hvis en uhåndteret fejl opstår uden at blive
grebet (Ej heller af global exception håndtering), så bliver status koden 500 sendt tilbage til klienten. Altså den viser ikke 
stacktracen som udgangspunkt (Dette kan man dog godt opleve i andre frameworks).

I den sidste øvelse skal _Developer exceptions pages_ anvendes.
  
1. I i metoden `Configure`, i klassen _Startup_,skal du indsætte koden `app.UseDeveloperExceptionPage();` i starten af koden.
2. Eksekver applikationen, og frem provoker en fejl med `'` eller `;`
3. Noter dig hvilken fejl der opstår?

Developer exception code er en bekvemlighed for udvikler der udvikler på applikationen, Dette bør kun anvendes i 
Udviklings miljø'er. Det hænder dog nogen gange at dette ved en fejl også bliver brugt i produktions miljøer, F.eks.
på grund at forkerte [miljø variabler](https://learn.microsoft.com/en-us/aspnet/core/fundamentals/environments?view=aspnetcore-7.0).


