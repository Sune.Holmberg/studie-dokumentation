# Opgave 5 (Individuel øvelse) - Regular expression  

## Information
Formålet med denne øvelse, er at øge bevidstheden om tilgangen til opgave løsning, når
man står overfor en opgave der som udgangspunkt kan virke uoverskuelig, eller handler om et
emne som man endnu ikke er introduceret til.

**Alle hjælpe midler er tilladt**

## Instruktioner
I denne opgave skal i udarbejde et regular expression (Regex) som kan detekter mønsteret
for et dansk cpr nummer. Herefter skal i producerer et eksempel i C# som bruger Regex mønster, til
at verificere CPR numre.

_Når i har fundet et mønster, kan i teste det med [Regex 101](https://regex101.com/)_


## Links
[Regex 101](https://regex101.com/)


## Løsning 
``` ^(0[1-9]|[12]\d|3[01])(0[1-9]|1[0-2])\d{2}[-]?\d{4} ```


``` using static System.Net.Mime.MediaTypeNames;
using System.Text.RegularExpressions;

Console.WriteLine("skriv et cpr nr");

var input = Console.ReadLine();

// indput vores cpr nr

var regex = @"^(0[1-9]|[12]\d|3[01])(0[1-9]|1[0-2])\d{2}[-]?\d{4}";

Regex rx = new Regex(regex, RegexOptions.Compiled | RegexOptions.IgnoreCase);

// Vi ser om vores cpr nr overholder vores regex
MatchCollection matches = rx.Matches(input);


//  Her fortæller den om der er matches 
if (matches.Count == 1)
{
    Console.WriteLine("{0} matches fundet in:\n   {1}", matches.Count, input);
}
else
{
    Console.WriteLine("Det ikke et cpr nr");
}

// Report on each match.
foreach (Match match in matches)
{
    GroupCollection groups = match.Groups;
    Console.WriteLine("'{0}' gentagelser ved {1} og {2}",
    groups["word"].Value, groups[0].Index, groups[1].Index);
} 