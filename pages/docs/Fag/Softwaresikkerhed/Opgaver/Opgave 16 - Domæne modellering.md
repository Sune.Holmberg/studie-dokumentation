# Øvelse 16 - Domæne modellering

## Information
I denne øvelse skal i udarbejde en domæne model til applikationen i lavet i opgaven [36 - grundlæggende OOP med C#](./6_Grundlæggende_OOP_med_sharp.md).
Typisk udarbejder man domæne modellen ud fra virkelighedens domæne. Men da det vigtigste for jer er at opnå forståelse for samehængen mellem koden og virkelighedens
domæne, laver vi en _"Reverse engineering"_ . Altså i skal udarbejde en domæne model ud fra det domæne i tror koden kommer fra.

** I virkelighedens verden vil man typisk havde en domæne ekspert til give input, her kan i blot bruge jeres underviser istedet **


## Instruktioner  
1. Udarbejde en overordnet domæne model hvor alle entiteter/klasser fremgår, samt forbindelsen mellem hver entitet/klasser.
2. Tilføj attributer/værdi objekter til klassen / entiteten.
3. Ud fra jeres diagram, vurder om navngivning o.l. giver mening (både i diagrammet og i koden)
4. Ud fra jeres diagram, vurder hvad hver attribute  / entitet må indeholde af værdier.
5. Vurder om koden der er implementeret i c# overholder alle de regler der måtte være i domænet.

## Løsning

1. ![Domæne model](../../../billeder/Software-sikkerhed/Opgave%2016%20-%20Domænemodel.png)

2. Se billed ovenfor

3. Navngivningen giver tildels mening for hvad de forskellige attributter/metoder skal bruges til 

4. Der er intet indputvalidering 
- BankAccount
    - _allTransactions: List
    - s_accountNumberSeed: int
    - Number: string (burde være en int eller decimal)
    - Owner: string 

- Transaction
    - Amount: decimal
    - Date: Datetime
    - Notes: string 

5. Koden overholder de regler som er i domænet, da domænet er blevet defineret efter koden er blevet lavet.