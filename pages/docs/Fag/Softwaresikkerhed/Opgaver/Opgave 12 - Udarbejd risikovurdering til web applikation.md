# Øvelse 12 - Udarbejd risikovurdering til web applikation

## Information
I denne øvelse skal der udføres risikovurdering til alle de misbrugstilfælde der blev
udarbejdet i forrige opgave.

Tilgangen til risikovurderinger i kontekst til faget _software sikkerhed_, bygger på en
pragmatisk tilgang til risikovurderinger målrettet software udviklings teams. Målet er derfor
ikke at havde den præcise viden, men ca. estimater og løbende forberede risikovurderingen 
gennem nye erfaringer/viden(iterativ process), samt det kontinuerlige arbejde med risikovurderinger i udviklingsprocessen.

Dialogen teamet har i forbindelse med risikovurderingen er også meget  værdifuld. Nogen risikoer
vil være svær at fastslå, F.eks. hvad vil konsekvensen være for forretningen i tilælde af Misbrugstilfælde
X bliver udført? Dette vil føre til et spørgsmål som teamet ikke kan svare på, og derfor at nød til at søge
opklaring på, hos forretningen eller egentlige risiko analytiker. Det vil være et åben spørgsmål som teamet
nu ved, at de er nød til at søge viden om.

## Instruktioner  
1. Udarbejd kvalitativt en skala for konsekvens
2. Udarbejd kvalitativt en skala for sandsynlighed
3. Benyt skalaerne fastslå sandsynlighed og konsekvens for hvert misbrugstilfælde.
4. Beregn risikoen ved hvert misbrugs tilfælde med formlen _Risiko=Sandsynlighed*Konsekvens_


## Løsning 
![Skala](../../../billeder/Software-sikkerhed/Risikovurdering%20opgave%2012.png)

