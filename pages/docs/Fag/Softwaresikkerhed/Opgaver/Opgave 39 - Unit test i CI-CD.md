# Øvelse 39 - Unit tests i CI/CD

## Information

I denne øvelse skal du tilføje unit test til din build pipeline.

Som tidligere vist i undervisningen kan unit tests understøtte sikkerheden i software, da en unit test hurtigt kan påvise en 
uhensigtmæssig ændring i koden, som potentielt kan introduceres en sårbarhed i softwaren. Dog har vi i undervisningen manuelt skulle
eksekverer disse unit test for at få påvist fejlen, hvilket er uhensigtmæssigt da en udvikler kan glemme at eksekverer disse efter en
ændring i koden. Build pipelinen kan sikre at alle disse unit tests eksekverees når der skubbes ændret kode ind i repositoriet. Hvis
nogen af unit testene fejler, afbrydes processen, og applikationen bliver ikke deployeret. Samtid bliver udvikleren underrettede om at 
nogen af testene har fejlet. 

I øvelsen skal du fortag ændringer i github actions konfigurations filen. Disse ændringer er dokumenteret [på Github docs](https://docs.github.com/en/actions/automating-builds-and-tests/building-and-testing-net#building-and-testing-your-code)

## Instruktioner
1. I konfigurations filen, tilføj følgende trin, inden build trinet:
    ```
    - name: Install dependencies
      run: dotnet restore
    - name: Execute unit tests
      run: dotnet test
    ```
_Yaml filer kan være en smule skrøbelige ift. indrykning o.l. så du skal måske rette i det et par gange, inden det virker_
2. Push til Github
3. verificer at alle trinene i build processen eksekveres uden fejl.

I dette konkrette tilfælde, sikre unit testene af model objekter opretholder deres invarians(Disse tests implementeret du tidligere). Såfremt invariansen
ikke længere opretholdes fejl unit testene. Så Invarians er et ekstra lag af sikkerhed, såfremt input valideringen skulle fejle. Samtid er unit testene endnu
et lag af sikkerhed såfremt invariansen skulle fejle. Altså der er nu helgarderet imod evt. uhensigtsmæssige ændringer i koden som kan føre til ikke valid data
i software.

I de næste trin skal du bevidst få unit testene til at fejle, for at obsever hvordan dette håndteres.

1. Udkommenter invariansen i domæne primitiverne.
2. Eksekver unit testene lokalt, og verificer at de fejler.
3. Push koden til github.
4. Observer build job. Hvorledes håndteres en fejlet unit test? afbrydes processen? bliver deployment trinene udført?

Du bør nu havde en nogenlunde overblik over hvordan unit tests kan bidrage til sikkerheden
i software. I de sidste trin skal der blot "ryddes op" i applikationen.

1. Fjern udkommenteringen af invariansen i domæne primitiverne.
2. Eksekver unit testene lokalt, og verificer at de ikke fejler.
3. Push koden til github.
4. Observer build job. Og verificer at alle trin eksekveres uden fejl.
