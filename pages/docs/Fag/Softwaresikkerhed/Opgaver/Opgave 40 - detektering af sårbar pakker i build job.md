# Øvelse 40 - detektering af sårbar pakker i build job

## Information
I denne øvelse skal der arbejdes med detektering af sårbar pakker i build pipelinen. Formålet er at vise
hvorledes detekteringen af sårbar pakker kan håndteres i en build pipeline.

Detektering af sårbar pakker bør egentlig ikke udføres af build pipelinen, men af repository hosten.
Github har [dependabot](https://docs.github.com/en/code-security/dependabot), der løbende kan skanne et repository
for sårbar pakker(pakker med en eksisterende CVS). I praksis er det bedst at anvende repositoriets indbygget sårbarheds
skanner såsom dependabot. Men dependabot kan  med det mindste interval, kun eksekveres 1 gang dagligt, hvilket ikke gør den 
særlig anvendelig til i undervisnings kontekst, defor anvendes denne ikke i øvelsen

Løbende automatiske skanninger for sårbar pakker er bestemt den bedste løsning. Men kan suppleres af såbarheds check når
der integreres ny kode ind i repositoriet. Det skal implementeres i denne øvelse.

**OBS! læs**
`dotnet list package --vulnerable --include-transitive` returner status kode 0 uagtet om den finder sårbar pakker eller 
ej. Hvilket gør den mindre hensigtsmæssig i et github actions build job, da processen kun afbrydes hvis applikationen ikke retuner status kode 0.
Derfor har undertegnet været nød til at være kreativ for at få dette til at fungerer. Løsningen bør kun anvendes i undervisningen, og kun med det
formål at vise konceptet!  
_Prøv og se, om du kan se hvilket potentiel problem løsningen kan skabe når du løser opgaven_


## Instruktioner
1. kopier projekt fra [dette repo](https://github.com/brunobritodev/ASP.NET-Core-SQL-Injection-Example) ind det repo du oprettede i forrige øvelse(Det er blot folderen `SqlInjection` du skal kopier ind)
2. Tilføj projektet til solution filen (dotnet sln add <path to csproj file>)
3. I Github actions konfigurations filen, tilføj følgende, efter trinet _Install dependencies_:
```
    - name: check for vulnerable packages
        run: |
          New-item vulnerabilities.txt
          dotnet list package --vulnerable --include-transitive | Tee-Object vulnerabilities.txt
          $matches = Get-Content vulnerabilities.txt | Select-String -Pattern "has the following vulnerable packages"
          if($matches.Length -gt 0){echo 'vulnerable packages detected'; exit 1}
```
4. Push koden til github
5. Observer build job, hvilken sårbarheder bliver der detekteret? afbrydes processen?
