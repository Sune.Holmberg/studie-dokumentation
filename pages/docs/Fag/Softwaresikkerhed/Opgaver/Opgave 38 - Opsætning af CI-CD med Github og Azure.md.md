# Øvelse 38 - Opsætning af CI/CD med Github og Azure web service.

## Information
Formålet med denne øvelse er at give en grundlæggende introduktion til konceptet bag _continuous integration_ og _continuous deployment_ (CI/CD).

I øvelsen skal du integrer koden fra en .Net application ind i et github repository, for derefter at lave et _build_ af applikationen og deployerer den
til Azure app service.

## Forudsætning for øvelsen.
For at kunne gennemfører øvelsen skal du havde følgende 2 kontoer oprettede:  
- Github konto, du kan oprette en Github konto [her](https://github.com/join)  
_Med din skole konto kan du oprette en student [developer konto](https://education.github.com/pack), som giver en række fordele, blandt flere build minutter_  
- En Azure studie konto. (Du har en tildelt med din UCL konto, du kan finde vejledning til denne på UCL guides https://www.ucl.dk/teaching/vejledninger/guides)  

Instruktioner til udførelse af opgaven kommer i flere underafsnit i instruktioner. Det er vigtig at du læser instruktionerne grundigt da øvelsen er en del større
end dem vi typisk arbejder med i faget. 

Du kan finde en general tutorial til [Azure web app her](https://learn.microsoft.com/en-us/azure/app-service/quickstart-dotnetcore?tabs=net70&pivots=development-environment-vs).
Du kan finde en general tutorial til [Azure web og github workflows her](https://learn.microsoft.com/en-us/azure/app-service/deploy-github-actions?tabs=applevel).

Generelt gennemgår øvelse det basale for opsætning af Github workflows og Azure web service. Men ønsker du yderlige viden kan du læse de to overstående tutorials.
Herudover er Microsoft azure dokumentationen samt Github informationen gode kilder.

## Instruktioner

### Opret repository på Github.
1. Opret et nyt repository på Github
2. Klone det nye (tomme) repository til din pc.
3. Kopier projekterne fra [Øvelse 36](https://ucl-pba-its.gitlab.io/23e-its-software-sikkerhed/exercises/36_Testing_Invarians/) ind i det tomme lokale repository (Applikationen, unit test samt solution)
**Indholdet af repo folderen skal være de 2 projekter. Web applikationen samt Unit test projektet, og en solution file. Du skal ikke kopier .git folderen over  i det tomme repository, da denne allerede har sin egen .get folder**
4. Push koden til Github.

### Opret Azure Web service
1. til [Azure create hub](https://portal.azure.com/#create/hub)
2. Vælg `Web app`
3. Følgende configurationer skal vælges på:
    - Vælg `Create new` til rescource group
    - Vælg `Azure for students` som abbonement
    - Vælg `code` i publish
    - Vælg `Net 7 (STS)` som runtime stack.
    - Vælg `Windows` som operativ system (Med Linux som OS, kan man ikke automatisk lave en integration til Github repo'et) 
    - Vælg `North Europe` som region.
    - Vælg `Free F1` som pricing plan  
4. Klik `Next deployment`
**Den gratis plan, giver 60 minutters dagligt oppetid på web app'en, så husk at slukke den når den ikke er i brug**
5. Følgende configurationer skal vælges i deployment:
    - Vælg `Enable github actions`
    - Vælg `Authorize Github`
        - Vælg det repository du tidligere lavet på Github
        - Vælg `Main` som branch.
    - Vælg `Preview file`, og orienter dig i konfigurations filen som bruges til opsætning af Github actions.
6. Klik `Next Networking`
    - Vælg `Enable` public access
    - Vælg `off` til _Network injection_
7. Klik `Next Monitoring`
    - Vælg `off` til application insights
8. Klik `Next tags`.
9. Tilføj et tag med navnet `type` og værdien `POC`. _Tags kan hjælpe med at søge gennem alle de resourcer man har på Azure_ 
10. Klik `Next: Review + create`
11. Verificer at opsætning stemmer overens med det angivet i øvelsen.
12. Klik `Create`
13. Afvent at Azure er færdig med at oprette resourcen, dette tager gerne 5-10 minutter den første gang at web appen oprettes.
14. Gå til Github repositoriet og klik på fanen `Actions`, her bør du kunne se build processen. (Som nævnt i forrige trin kan det godt tage lidt tid den første gang, også inden build processen går igang)
15. Klik ind på workflowet, og se hvilken _jobs_ der bliver eksekveret.
16. kig i filen `.github\workflows\master-<appnavn>.yml` og verificer at de eksekveret trin i jobbet stemmer overens med de trin der blev eksekveret i build jobbet.
17. Gå tilbage til `web app overview` på Azure og klik på  linket under `Default domain`. Verificer at denne før jer til appen.
18. Stop app eksekvering, således i ikke unødigt bruger den begrænset daglige tid.

Du har nu lavet en CI/CD build pipeline. I de næste par øvelser kobler vi dem sammen med tidligere emner fra undervisningen.


