# Øvelse 18 - Input validering til konsol applikation.

## Information
Formålet med denne øvelse, er at du anvender viden om Regex fra forrige øvelse, til at implementerer
input validering af bruger inputtet i en konsol applikation.

I denne øvelse, samt øvelse anvendes repositoriet: [https://github.com/mesn1985/InputValidationsBasicExercises](https://github.com/mesn1985/InputValidationsBasicExercises)

I repositoriet som du skal klone er der 3 projekter. Den du skal arbejde med i denne øvelse er
projektet `ValidateTheName` som er et konsol projekt.
I projektets `Program.cs` file er der implementeret en metode ved navn `nameInputIsInvalid`, hvor i 
du skal implementere validerings logikken som bliver beskrevet i nedstående Instruktioner.

Som beskrevet i forberedelsen til idag, skal man altid kontroller længde af tekst(string) inden man
anvender Regex til at detekter mønstre. Hvis teksten hvorpå der anvendes Regex er for stor, kan det 
resulterer i en udtømning af applikationens ressourcer.
  
Hvis det grundlæggende C# skal genopfriskes, kan du med fordel referer tilbage til din løsning og noter fra [Opgave 6](6_Grundlæggende_OOP_med_sharp.md)

Du kan finde hjælp til at finde [længden på tekst strenge her](https://learn.microsoft.com/en-us/dotnet/api/system.string.length?view=net-7.0),
og til [if statements her](https://learn.microsoft.com/en-us/dotnet/csharp/language-reference/statements/selection-statements)

Til Regex'et der skal udarbejdes kan du bruge google som hjælp, og teste regex udtrykket med forskellige strenge
på [Regex 101](https://regex101.com/).

Du kan ekseverer og teste applikationen løbende, F.eks. efter hver trin i `Instruktioner`.

## Instruktioner  
1. Valider at navnet i argumentet `nameInput` ikke er længere 20 karakterer
2. Valider at navnet i argumentet ikke er kortere end 3 karakter
3. Valider at navnet kun indeholder alfabetiske karakter, med Regex (ikke andre tilgange).
4. Sikker dig at validering virker ved at eksekver applikationen, og afprøv med et par forskellige inputs.
_Husk og bibeholde rækkefølgen, således at længden bliver valideret inden i valider tekst strengens indhold_


![Løsning](../../../billeder/Software-sikkerhed/Opgave%2019.png)