# Øvelse 17 - Introduktion til regex i C#

## Information
Formålet med denne øvelse er at give en grundlæggende introduktion
til `Regular expressions (Regex)` samt dennes anvendelse i C#.

Du skal følge [Tutorial](https://www.csharptutorial.net/csharp-regular-expression/csharp-regex/) for at
få en grundlæggende introduktion til anvendelsen af regex i C#. Når du har fulgt og implementeret tutorialen,
kan du med fordel eksperimenter ved at lave ændringer i det implementeret, dette vil øge forståelsen for 
Regex.

Overordnet er Regex en måde hvorpå man kan definerer mønstre i tekst. Det vil sige man kan benytte Regex til at 
detekter om en tekst(F.eks. C# string) overholder et givet mønster, F.eks om teksten kun indeholder karakter, eller
kun indeholder alfabetiske karakter. Dette gør Regex særdeles velegnet til input validering af data. 

**Det er ikke forventet, at du på stående kan huske Regex syntaksen på noget tidspunkt, kun at du kan redegøre for det regex du har implementeret**
For 99% af alle mennesker er Regex syntaks et opslagsværk, Når man skal udvikle en nyt Regex udtryk kan man anvende tidligere udarbejdet 
Regex udtryk (Finde dem via Google F.eks.) og teste dem med et værktøj som F.eks. [Regex 101](https://regex101.com/) 

Hvis det grundlæggende C# skal genopfriskes, kan du med fordel referer tilbage til din løsning og noter fra [Opgave 6](6_Grundlæggende_OOP_med_sharp.md)

## Instruktioner  
1. Opret en ny .Net konsol applikation ved navn `BasicRegexUsage`
2. Følg og implementerer [Tutorialen](https://www.csharptutorial.net/csharp-regular-expression/csharp-regex/)
