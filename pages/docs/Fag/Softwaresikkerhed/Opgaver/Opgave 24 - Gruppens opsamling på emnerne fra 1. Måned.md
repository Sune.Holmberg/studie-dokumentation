# Øvelse 24 - Gruppens opsamling på emnerne fra 1. Måned

## Information
Formålet med denne øvelse er vidensdeling og overblik internt i gruppen ift. til de emner,
der er blevet gennemgået på semesterets 1. Måned.

Erfaringer fra tidligere semestere har vist at dette kan være en god ide, da gruppen til sidst i 
semesteret skal lave eksamens projekt sammen.

Gruppen skal sammen at forsøge og skabe overblik over de emner der er blevet gennemgået indtil videre.
Her kan der med fordel tages udgangspunkt i fagets plan på _It's learning_. Herfra kan de overordnet emner noteres
ned (På F.eks. et Whiteboard), herefter kan underemner,øvelser o.l. noteres ned.

Herudover bør gruppen forsøge at skabe overblik over hvilken læringsmål de har været i berøring med i løbet
af undervisningen, og forsøge at knytte læringsmålene til konkrette emner.

Dette er dog kun forslag, den process der understøtter gruppen bedst kan anvendes. Det vigtige er at der bliver
skabt overblik i gruppen.
