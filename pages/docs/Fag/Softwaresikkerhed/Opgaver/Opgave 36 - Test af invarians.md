# Øvelse 36 - Test af invarians.

## Information
Formålet med denne opgave at introducerer unit testing af objekters håndhævelse af invariance.

I opgaven skal du implementer unit tests til domæne primitiverne _Firstname_ og _Lastname_ som du implementeret i
[Opgave 26](./26_Person_with_domain_primitivesWebApplication.md).

Den metode som skal tests er constructor kaldet. Du kan se et eksempel på validering af kastet exception [her](http://dontcodetired.com/blog/post/Testing-for-Thrown-Exceptions-in-xUnitnet). Og et eksempel på validering af at der ikke bliver kastet exception [her](https://peterdaugaardrasmussen.com/2019/10/27/xunit-how-to-check-if-a-call-does-not-throw-an-exception/).
  
Når vi skal arbejde med build pipelines i en senere lektion, kigger vi på hvordan unit tests kan hjælpe med at opretholde sikkerheden i software.
     
I opgaven skal du huske og flytte projektet fra [Opgave 26](./26_Person_with_domain_primitivesWebApplication.md), ind i et biblotek, og tilføj en solution, ligesom
i tutorialen i forrige opgave. 
  
## Instruktioner
1. Opret en test klasse til _Firstname_ domæne primitivet
2. Lav i test klassen, en test metode der validerer at der *ikke* bliver kastet en exception når reglerne for lægenden på navnet bliver overholdt.
3. Lav i test klassen, en test metode der validerer at der *ikke* bliver kastet en exception når reglerne for hvilken karakter navnet må indehold bliver overholdt.
4. Lav i test klassen, en test metode der validerer at der bliver kastet en exception når reglerne for længden på navnet ikke bliver overholdt. (For mange eller for får karakterer)
5. Lav i test klassen, en test metode der validerer at der bliver kastet en exception når reglerne for hvilken karakter navnet må indeholde ikke bliver overholdt(test både med tal og speciel karakter)
6. Lav i test klassen, en test metode der validerer at der bliver kastet en exception når objektet bliver initialiseret med null.
  
Input validering i Controller klasserne kan også unit tests, dette kan du læse mere om [her](https://learn.microsoft.com/en-us/aspnet/core/mvc/controllers/testing?view=aspnetcore-7.0)


