# Øvelse 8 - (Gruppe øvelse) udarbejdelse af system mål fra borger.dk

## Information
Formålet med denne øvelse er at gruppen udarbejder system mål fra borger.dk.
Grunden til at borger.dk anvendes er for at fremhæve det abstraktions(Detaljegrad) niveau 
system målene som udgangspunkt bør havde, altså systemet skal ses som en _black box_, 
hvor i kun kan se hvad systemet kan, ikke hvordan(Ingen tekniske detaljer). 

**Husk at fokuset er på hvilken værdiskabelse systemet lever til aktøren og forretningen**

### Kontekst
Når der arbejdes med store systemer, er det vigtig at man udvælger den konkrete kontekst
man ønsker at arbejde med. Når man bruger brugstilfælde til at definere system målene,
kan man indledningsvist udvælge en kontekst ved at udvælge hvilken aktør der skal understøttes.
Et eksempel kunne være at aktøren er "Løn modtager". Så kan nogle af system målene findes under "Feriepenge"


## Instruktioner  
1. Inde på [borger.dk](https://www.borger.dk/)
2. Udvælg hvilken kontekts af systemet i vil fokuser på (F.eks. ved at vælge en enkelt aktør)
3. Definere som minimum 3 system mål med brugstilfælde. **Brug eksemplet i dagens forberedelse som vejledning**
4. Udarbejde til sidst et brugstilfælde diagram. **Brug eksemplet i dagens forberedelse som vejledning**

## Løsning 
Titel: Find SU Informationer		Aktør: Studerende

En studerende tilgår borger.dk for at søge SU, han / hun bliver taget til en oversigtsside omkring SU, Borgeren skal klikke på “Log på min SU”, for at komme videre. Borger bliver herefter bedte om at logge ind med MitID, hvor man bliver videreført til su.dk.

Titel: Søg Kontanthjælp		Aktør: Borger

En borger tilgår borger.dk for at søge kontanthjælp, han / hun skal først vælge hvilken kommune som borgeren tilhører. Efterfølgende skal borgeren henvende sig fysisk hos kommunen på første ledighedsdag. Som supplering til ansøgningen, kan man udfylde en blanket, blanketten bliver dog afvist, hvis man ikke melder sig fysisk. 


Titel: Se din nuværende pension	Aktør: Ældre Borger

En ældre borger skal tilgå sin pension. Borgeren navigerer til borger.dk for at finde oplysning omkring deres pension. De bliver mødt af en pensionsportal, hvori der står diverse informationer. De tilgår siden “min pension” for at finde overblik, og bliver videreført til MitID siden, hvor de skal identificere sig. 

Sucess / fejl scenarie – ekstern aktør (ældre befolkning)
