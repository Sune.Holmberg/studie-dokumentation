# Øvelse 28 - Detektering af sårbar afhæninger med Dotnet CLI værktøjet

## Information
Formålet med denne øvelse er at introducerer 3.parts sårbarheder og CVE'er.
Det meste software idag bygger på software andre har lavet tidligere. Dette importes
i projekter, og omtales ofte pakker, eller eksterne bibloteker.

Når man anvender eksterne bibloteker importer man noget kode som man ikke selv har 
udviklet og ikke nødvendigvis har hverken indflydelse eller indsigt i. Dette kan være
problematisk hvis der opstår en sårbarhed. Heldigvis findes der `CVE` databaser som indeholder
data om kendte sårbarheder i software. Et eksempel er [CVE.org](https://www.cve.org/).

I .Net importer man eksterne bibloteker som `Nuget packages`. Man kan se en liste over alle anvendte
eksterne bibloteker i projekts projekt file som altid slutter på `.csproj`.

I denne øvelse skal du anvende CLI værktøjet `Dotnet` til at skanne et .Net projekts `Nuget packages`
for sårbarheder.

Du skal klone repo [https://github.com/tobyash86/WebGoat.NET](https://github.com/tobyash86/WebGoat.NET) som
er et bevidst sårbart Asp .Net core projekt.

## Instruktioner
1. Gå ind i folderen til det klonet repository.
2. Følg instruksen fra denne [tutorial](https://devblogs.microsoft.com/nuget/how-to-scan-nuget-packages-for-security-vulnerabilities/) for at udføre en 3. Parts sårbarheds skanning(Se afsnittet dotnet CLI)

![Billed](../../../billeder/Software-sikkerhed/Opgave%2028%20liste.png)

![billed](https://devblogs.microsoft.com/nuget/wp-content/uploads/sites/49/2021/03/2021-03-01_9-40-10.png)

3. Følg linket til den fundende sårbarhed, og noter CVE nummeret(i toppen af siden fra linket).
- CVE-2022-41064
4. Slå CVE'en op på [CVE.org](https://www.cve.org/). Kan der findes mere information om CVE'en?
- https://www.cve.org/CVERecord?id=CVE-2022-41064
- Ja, det kan der

5. Brug `--include-transitive` for at se under sårbarhederne. (Se tutorial for yderlige information)
![billed](https://devblogs.microsoft.com/nuget/wp-content/uploads/sites/49/2021/03/2021-03-01_18-26-59.png)
