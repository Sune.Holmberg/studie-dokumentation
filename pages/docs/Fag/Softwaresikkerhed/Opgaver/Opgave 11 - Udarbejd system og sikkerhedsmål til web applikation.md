# Øvelse 11 - Udarbejd system og sikkerhedsmål til web applikation

## Information
I denne øvelse, skal i udarbejde system mål og sikkerheds mål til en web applikation.

Applikationen i skal udarbejde system og sikkerheds mål til kan findes her: [.Net reference webshop](https://github.com/dotnet-architecture/eShopOnWeb).
Brug værktøjet _git_ til at klone reposistoriet med, og start applikationen ved at følge instruktionerne i readme afsnittet _Running the sample locally_.

I øvelsen skal der udarbejdes system mål repræsenteret i brugstilfælde format, og herefter skal der afledes
sikkerheds mål ud fra hver system mål.
   
****

**Husk at holde det korrekte abstraktions niaveu i brugstilfældene, altså de skal vise hvordan systemet opfylder et forretningsmæssigt formål**  
  
**Husk at anvende CIA når sikkerhedsmål udledes fra system mål**

## Instruktioner  
1. Identificer titlerne til 3 brugstilfælde, og noter dem på et brugstilfælde diagram.  
_Bemærk der står identificer, ikke udarbejd, her er det nok blot at havde brugstilfælden noteret på et brugstilfælde diagram_ .
2. Ud fra brugstilfælde diagrammet skal i nu brainstorme og identificer misbrugstilfælde, identificer som minimum 3 misbrugstilfælde, og noter dem på brugstilfælde diagrammet.  
_Bemærk at der stadig står identificer, ikke udarbejd_  
3. Udarbejd nu som minimum 2 brugstilfælde.  
_At udarbejd et brugstilfæde betyder at brugstilfældet nu skal beskrives_  
4. Udarbejd nu som minimum 1 misbrugstilfælde for hvert udarbejdet brugstilfælde.  

## Løsning 

- Identificering = Et overordnet billed.
- Udarbejdelse = En detaljeret beskrivelse.

![Diagram](../../../billeder/Software-sikkerhed/Opgave%2011%20-%20diagram.png)

### 1. Identifikering af brugstilfælde
- Brugstilfælde 1 - Tilføj produkter til indkøbskruven

- Brugstilfælde 2 - Filterer produkter 

- Brugstilfælde 3 - Se sin indkøbskruv

### 2. Identifikering af misbrugstilfælde
- Misbrugstilfælde 1 - Tilføj minusvarer

- Misbrugstilfælde 2 - Se varer som ikke er tilgængelige 

- Misbrugstilfælde 3 - Se andres indkøbskruv

### 3. Beskrivelse af brugstilfælde 
Det er meget vigtigt at man laver en detaljeret beskrivelse af de forskallige cases.

#### Brugstilfælde 1 - Tilføj produkter til indkøbskruven
Man skal kunne tilføje produkter til sin indkøbskruv. Når man har tilføjet varen til indkøbskruven, skal man kunne se antallet af den bestemte type vare som man har tilføjet til indkøbskruven, samt prisen. 

#### Brugstilfælde 2 - Filterer produkterne 
Man skal kunne lave et filter på hvilke varer som man vil kunne se på sin skærm. Man skal kunne filtærer efter prisen, efter hvad der er på lager, samt filtærer efter hvad det er for en type varer og hvilket mærke varen er, som man vil se. 

### 4. Beskrivelse af misbrugstilfælde
#### Misbrugstilfælde 1 - Tilføj minusvarer
Det er muligt at kunne tilføje minusvarer, hvilket der får det til at se ud i systemet, som om brugeren skal have penge tilbage. 
For at kunne stå i mode det, skal man lave noget indputvalidering, hvor det ikke vil være muligt at kunne tilføje andet end 0 og op. 