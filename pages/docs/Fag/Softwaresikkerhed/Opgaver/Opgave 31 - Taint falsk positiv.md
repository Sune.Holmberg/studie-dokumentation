# Øvelse 31 - Taint analyse med falsk positiv

## Information
Taint analysen vurderer om data er _tainted_ ved at følge input data gennem alle dets mulige vej i koden.
Hvis taint analysen ikke vurder at udvikleren som har skrevet koden har lavet en aktiv og valid foranstaltning
for at sikre at dataen er valid, vil dataen aldrig blive vurderet som _untainted_. Bliver den _tainted_ data lagt
sammen men noget anden data, vil den sammenlagte data nu blive betragtet som værende tainted. Et eksempel på dette
ses i koden fra forrige opgave:  
```Csharp
var query = "SELECT FirstName, LastName FROM Student WHERE FirstName Like '%" + name + "%'";
```  
Som det ses sammentrækkes `name` med andre tekst strenge, og den resulterende tekst streng `query` bliver
derfor betragtede som værende _tainted_. Hvilket var det taint analyse reageret på i forrige afsnit.

En naturlig tilgang vil være at lave en input validering af name, ud fra den kontekst udvikleren vurder at 
dataen ikke længere er _tainted_. Men da taint analyse værktøjet som udgangspunkt ikke har nogen forudsætning
for at vurderer hvornår data bør betragtes som sikker i en kontekst bestemt situation, vil den stadig betragte
dataen som værende _tainted_, og derfor reagere. Dette kaldes en falsk positiv. Altså taint analyse vurder data
som værende forurenet selv det ikke er tilfælde.  
_Mange taint analyse værktøjer kan konfigures til ikke at reagerer i specifikke tilfælde, dette dækker vi dog ikke i undervisningen_

I denne øvelse skal du fremkalde en falsk positiv i taint analyse. Projektet der arbejdes med, er projektet fra forrige opgave.

Du skal implementer input validering i metoden `SearchStudentUnsecure`.

Fremgangsmåden for validerings metode er magen til den du anvendte i [Opgave 19](19_Input_validering_med_post_form.md)

## Instruktioner
1. I klassen `HomeController`, opret en privat metode til validering af en tekst streng. Teksten må kun indehold alfabetiske karakter.
2. I metoden `SearchStudentUnsecure` skal du nu bruge validerings metoden, til at valider tekst strengen _name_.   _Validerings metoden skal være den første metode der bliver kaldt_
3. Eksekver nu taint analysen med Snyk. (tilsvarende forrige opgave)
4. Noter om _SQL Injection_ stadig detekteres som en sårbarhed.

- Snyk detektere stadig koden, selvom der er blevet lagt en invarians validering af dataen ind.