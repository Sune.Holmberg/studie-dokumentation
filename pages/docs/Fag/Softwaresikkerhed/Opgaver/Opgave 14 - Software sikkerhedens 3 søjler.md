# Øvelse 14 - Software sikkerhedens 3 søjler

## Information
Formålet med denne øvelse er at lave en opsamling på de 4 
tidligere emner som er: System mål, Sikkerheds mål, risiko vurderingen
samt trussels modellering. Alle disse bør indgå udviklingsprocessen,
og er beskrevet i 2 af software sikkerhedens 3 søjler(Anvendt risikostyring & berøringspunkter).
Disse 3 søjler skal i nu arbejde med.

**Husk og noter besvarelserne ned**

## Instruktioner  
I øvelsen skal gruppen sammen lave en "mapping" software sikkerhedens 3 søjler 
over til hvert af de 4 tidligere emner. Altså hvilket emne høre til i hvilken søjle.
Herudover skal der laves en  "mapping" fra software sikkerhedens 7 berøringspunkter over til
de emner berøringspunkterne passer sammen med.
Herudover skal gruppen lave et overblik over de berøringspunkter der 
ikke bliver afdækket af de tidligere emner. Så opsummeret skal følgende laves:  
  
- En mapping fra software sikkerhedens 3 søjler til de 4 tidligere emner: System mål,sikkerheds mål risikovurdering og trusselsmodellering.  
- En mapping fra de 4 tidligere emner over til de 7 berøringspunkter.
- Et overblik over hvilken af de 7 berøringspunkter der ikke bliver afdækket.
  
**I kan søge hjælp i Introduktion til software sikkerhed del 6 som var forberedelse til idag**

## Løsning

### Søjlerne
- Anvendt risikostyring
    - System mål
    - Sikkerheds mål
    - Risikovurdering
    - Trusselsmodellering

- Berøringspunkter
    - System mål
    - Sikkerheds mål

- Viden/vidensdeling
    - Risikovurdering
    - Trusselsmodellering

### De 7 berøringspunkter
- Misbrugstilfælde
- Sikkerhedskrav
- Risiko analyse af arkitektur
- Review af kode
- Penetration testing
- Risiko baseret sikkerheds test
- Sikkerheds operationer

### Scrum
- Sprint planning
    - Systemmål 
    - Sikkerhedsmål 

- Sprint
- Sprint review 
- Sprint retrospective

- Backlog refinement
    - Systemmål 
    - Sikkerhedsmål 

- Misbrugstilfælde
    - Berøringspunkter
    - Systemmål
    - Sikkerhedsmål

- Risiko analyse
    - Anvendt risikostyring
    - Risikovurdering
    - Trusselsmodellering

