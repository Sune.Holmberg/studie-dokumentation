# Øvelse 23 - Opretholdelse af objekters integritet med invariance i web applikationer

## Information
Formålet med denne øvelse er at vise hvordan Objekter der håndhæver deres invariance gennem en constructor kan anvendes i en web applikation,
til at sikre sikkerhed i flere lag.

Ofte vil forskellige mennesker arbejde på forskellige dele af en softwaren. Hvilket kan betyde at man måske tror at  noget (Såsom) input
validering bliver håndteret et andet sted. Uden at det faktisk gør det. Det kan gøre softwaren sårbart. Derfor bør man tænke defense in depth,
og sige at alt data softwaren modtager altid bliver valideret så hurtig som muligt, som du gjorde i Controller klassen i de forrige øvelser.
Men ligeså snart noget data bliver givet videre fra Controller klassen, til en anden klasse i software, så bliver det givet videre som et objekt
der opretholder invariance og derfor har integritet. Så opstår der aldrig tvivl om hvorvidt man kan stole på den data man modtager, internt i 
softwaren.

Typisk vil data som bliver afsendt af en anden applikation, have overskredet en tillidsgrænse og bliver derfor modtaget med en såkaldt `Data Transfer Object`(som i [øvelse 20](20_Input_validering_og_model_binding.md)). DTO har som ofte ikke nogen regler tilknyttet, udover hvilken type data den må indeholde. Derfor bør en dto aldrig forlade Controller klassen.
I denne øvelse vises der et (meget primitivt) eksempel på dette.

Du skal i denne øvelse orienterer dig om hvordan man anvender overgange fra et modtaget DTO objekt, til et objekt der håndhæver invarians.

Projektet du skal anvende er `InvarianceDTOandModel` fra repo'et [https://github.com/mesn1985/InputValidationsBasicExercises](https://github.com/mesn1985/InputValidationsBasicExercises),
som du klonet i en tidligere øvelse.

## Instruktioner
1. Inspicer metoden `ValidateName` i filen `NameController.cs`. hvorfor oprettes der et nyt objekt med værdierne fra det modtaget DTO objekt `person`?
2. Åben filen `Person.cs` som kan findes i folderen _Models_
3. Implementer håndhævelsen af invariance for de to argumenter i Person constructoren. (Reglerne er de samme som fra tidligere øvelser)
4. Afprøv applikationen, og valider mod forskellige inputs.  
_Bemærk at den eneste regel input valideringen håndhæver er at data skal være sat [Notnull]_
5. Hvad sker der når du forsøger med F.eks. et navn på 21 karakterer?
