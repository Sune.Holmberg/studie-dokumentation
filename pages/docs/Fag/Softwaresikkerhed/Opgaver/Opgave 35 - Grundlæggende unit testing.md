# Øvelse 35 - Grundlæggende unit testing.

## Information
Formålet med denne øvelse, er at give en grundlæggende introduktion til unit testing.
  
En del sårbarheder i software, opstår som følge af ændringer i koden. F.eks. hvis Udvikler X har 
skrevet noget kode, og udvikler Y så senere skal ind og ændre den kode. Så kender udvikler Y ikke
nødvendigvis omfanget eller meningen med den kode som udvikler X har skrevet. Det kan betyde 
at der uhensigtsmæssigt bliver introduceret sårbarheder i koden. Dette kan unit tests dog hjælpe med 
at forhindre. Unit tests kan nemlig teste enkelte metoder og derved kontroller om metoden agerer som tiltænkt.
Dette betyder at hvis udvikler X implementer en unit test for hver metode der er skrevet. Så kan Udvikler Y
eksekver disse unit tests. Er der så nogen af unit testene som fejler, bliver Udvikler Y gjort opmærksom på
at han har lavet en ændring der kan havde betydning for kodens funktion (F.eks. ift. sikkerhed).
  
Som en grundlæggende introduktion til Unit test skal du følge og implementere [denne tutorial](https://learn.microsoft.com/en-us/dotnet/core/testing/unit-testing-with-dotnet-test)

