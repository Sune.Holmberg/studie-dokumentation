# Øvelse 6 - Grundlæggende OOP med C sharp

## Information
Formålet med øvelsen er at sikre en grundlæggende forståelse for C# og de objekt orienteret programmering med denne.
Øvelsen tager udgangspunkt i en C# tutorial fra Microsoft, og en del af øvelsen er brugen af dokumentation.
  
Instruktionerne fungerer som en læse vejledning til tutorial, og fremhæver hvad du bør være særligt
opmærksom i hvert af afsnittene efter du har implementeret jvf. tutorial. Efter implementering af hvert afsnit, bør du læse 
vejledningen for hvert afsnit.

link til tutorial:  
[Tutorial](https://learn.microsoft.com/en-us/dotnet/csharp/fundamentals/tutorials/classes)

## Pratiske læringsmål med øvelserne
-   Den studerende oprette en klasse og forstår dennes anvendelse.
-   Den studerende anvende Constructor til initialisering af objekter.
-   Den studerende kan anvende Constructor til initialisering af objekter.
-   Den studerende har grundlæggende forståelse for Indkapsulering i objekter ved brug af private accessors.

## Instruktioner

### Prerequisites
I dette afsnit benævnes de grundlæggende værktøjer som kan anvendes til implementering
af software med C#. Visual studio er microsoft egen IDE(Integrated development enviroment), og
tilbyder en del støtte funktioner til implementering af software med C#. Visual studio code er
en text editor som også er udviklet af Microsoft. Denne kan udvides med en plugins, således at  
denne også kan fungere som IDE.  
SDK som nævnes er _Software Development kit_, og indeholder værktøjer som kræves for at udvikle
software med C#, F.eks. compiler (Roslyn), CLI værktøj (Dotnet) osv.  

### Create your application
I dette afsnit handler om oprettelse af projektet, som en konsol applikation.
I tutorialen anvendes `Dotnet new console` til at oprette projektet. Dette kan
også gøres i visual studio, som vist [her](https://learn.microsoft.com/en-us/visualstudio/get-started/csharp/tutorial-console?view=vs-2022)  
  
**Husk at projektet skal oprettes i en folder der hedder classes**  
  
### Define the bank acccount type
I dette afsnit skal du oprette filen _BankAccount.cs_ i projektet, og i filen
implementerer koden til klassen Bank Account. Overvej følgende punkter, efter implementeringen:

- Hvad er _Number_,_Owner_ og _balance_ ?
- Hvad er _MakeDeposit_ og _MakeWithdrawal_ ?
- Hvorfor omtales klasser også som typer?

### Open a new account
I dette afsnit implementeres en construtor der tager imod 2 pararmetere. Herudover
implementeres en privat statisk variable. Overvej følgende punkter, efter implementeringen:
  
- Hvorfor implementer en constructor som tager imod 2 pararmetere?
- Kan objektet stadig initialiseres uden pararmetere?
- Hvad betyder det at _s_accountNumberSeed_ er privat? (Kan variablen bruges uden for klassen, i F.eks. konsol applikationen?)

### Create deposits and withdrawals
I dette afsnit implementeres en ny type(klasse) _Transaction_. som herefter anvendes 
til udførelse deponering og udtræk. Overvej følgende  punkter, efter  implementeringen:
  
- Hvad er fordelen ved at transaktioner har sin egen type(klasse)?
- hvorfor er listen _allTransactions_ private? (indkapsulering)
- Hvad sker der hvis koden ` throw new ArgumentOutOfRangeException(nameof(amount), "Amount of deposit must be positive");` eksekveres?
- Hvorfor skal metoden _MakeDeposit_ kaldes i _Constructor_?

### Challenge - log all transactions
I dette afsnit implementeres en metode som kan generer en tekst streng, som viser 
hele transaktions historikken. Overvej følgende punkter:  
- Hvordan anvendes den private liste _alltransactions_ , og hvorfor kan denne tilgang være en god ide?


### Løsning 

#### Program.cs
```using Classes;

var account = new BankAccount("<name>", 1000);
Console.WriteLine($"Account {account.Number} was created for {account.Owner} with {account.Balance} initial balance.");
account.MakeWithdrawal(500, DateTime.Now, "Rent payment");
Console.WriteLine(account.Balance);
account.MakeDeposit(100, DateTime.Now, "Friend paid me back");
Console.WriteLine(account.Balance);

//// Test that the initial balances must be positive.
//BankAccount invalidAccount;
//try
//{
//    invalidAccount = new BankAccount("invalid", -55);
//}
//catch (ArgumentOutOfRangeException e)
//{
//    Console.WriteLine("Exception caught creating account with negative balance");
//    Console.WriteLine(e.ToString());
//    return;
//}

//// Test for a negative balance.
//try
//{
//    account.MakeWithdrawal(750, DateTime.Now, "Attempt to overdraw");
//}
//catch (InvalidOperationException e)
//{
//    Console.WriteLine("Exception caught trying to overdraw");
//    Console.WriteLine(e.ToString());
//}

Console.WriteLine(account.GetAccountHistory());

```

#### BankAccount.cs
```using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Classes;

    public class BankAccount
    {
        private List<Transaction> _allTransactions = new List<Transaction>();
        private static int s_accountNumberSeed = 1234567890;
    public string Number { get; }
        public string Owner { get; set; }

    public decimal Balance
    {
        get
        {
            decimal balance = 0;
            foreach (var item in _allTransactions)
            {
                balance += item.Amount;
            }

            return balance;
        }
    }

    public BankAccount(string name, decimal initialBalance)
    {
        Number = s_accountNumberSeed.ToString();
        s_accountNumberSeed++;

        Owner = name;
        MakeDeposit(initialBalance, DateTime.Now, "Initial balance");
    }

    public void MakeDeposit(decimal amount, DateTime date, string note)
    {
        if (amount <= 0)
        {
            throw new ArgumentOutOfRangeException(nameof(amount), "Amount of deposit must be positive");
        }
        var deposit = new Transaction(amount, date, note);
        _allTransactions.Add(deposit);
    }

    public void MakeWithdrawal(decimal amount, DateTime date, string note)
    {
        if (amount <= 0)
        {
            throw new ArgumentOutOfRangeException(nameof(amount), "Amount of withdrawal must be positive");
        }
        if (Balance - amount < 0)
        {
            throw new InvalidOperationException("Not sufficient funds for this withdrawal");
        }
        var withdrawal = new Transaction(-amount, date, note);
        _allTransactions.Add(withdrawal);
    }

    public string GetAccountHistory()
    {
        var report = new System.Text.StringBuilder();

        decimal balance = 0;
        report.AppendLine("Date\t\tAmount\tBalance\tNote");
        foreach (var item in _allTransactions)
        {
            balance += item.Amount;
            report.AppendLine($"{item.Date.ToShortDateString()}\t{item.Amount}\t{balance}\t{item.Notes}");
        }

        return report.ToString();
    }
}

```
#### Transaction.cs

```
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Classes;

public class Transaction
{
    public decimal Amount { get; }
    public DateTime Date { get; }
    public string Notes { get; }

    public Transaction(decimal amount, DateTime date, string note)
    {
        Amount = amount;
        Date = date;
        Notes = note;
    }
}
```