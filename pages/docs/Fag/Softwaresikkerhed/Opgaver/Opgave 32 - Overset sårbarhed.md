# Øvelse 32 - Overset sårbarhed

## Information
De fleste taint analyse værktøjer betragter data fra objekter som værende _untainted_ (I sprog som understøtter OOP). Dette kommer sig af  
at taint analyse værktøjet formoder at en _Constructor_ har været anvendt til at oprette objektet som indeholder dataen. Det er formodet praksis
at objekter håndhæver invarians i _Constructoren_ og derfor formodes data'en at være untainted. Dette skal efterprøves i denne opgave.

I projektet fra de to forrige opgaver skal du implementerer en klasse ved navn `Name`. Klasse skal have et _private  field_ til en tekst streng.
Klassens Constructor skal tage imod en tekst streng som argument, og initialiser tekst strengs _private field_.
**Det er vigtig at invarians undtagelsesvist ikke håndhæves, da øvelsen skal vise hvordan sårbarheder ikke opdages ved dårlige praksiser**
Klassen skal desuden havde en _public_ metode ved navn _GetName_, der return værdien fra _private field_ tekst strengen.
Udfør herefter opgaven ved at følge trine i instruktioner.

## Instruktioner
1. i metoden `SearchStudentUnsecure`, fjern eller udkommenter validerings metode fra forrige opgave.
2. i starten af metoden `SearchStudentUnsecure`, initialiser et 'Name' objekt, med argumentet _name_.
3. i tekst strenge sammentrækningen ```"SELECT FirstName, LastName FROM Student WHERE FirstName Like '%" + name + "%'"``` skal du udskifte _name_ med et kald til GetName metoden op `Name` objektet.
4. Eksekver Taint analyse med Snyk.(Som i de to forrige opgaver).
5. Hvad er grunden til at sårbarheden ikke længere bliver fundet?

- Den er ligeglad med valideringne, og den mælder den som en sårbarhed.
