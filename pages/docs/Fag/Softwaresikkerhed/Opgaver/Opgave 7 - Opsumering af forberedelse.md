# Øvelse 7 - (Gruppe øvelse) Fælles forståelse for system & sikkerhedsmål

## Information
Formålet med denne øvelse er at gruppen laver en fælles afstemmeles af forståelsen for
hvad system mål og sikkerheds mål er, samt tankegangen bag _Secure by design_. Dette træner brugen af 
termanonlogier samt fagbegreber. Dette er vigtig når man skal kommuniker internt i gruppen,
og i proffesionel samehængen(F.eks. på en arbejdsplads). Endvidere understøtter vidensdelingen
og forståelse for fag termerne samt begreber den  videre læring og eksaminationen. 


## Instruktioner  
Gruppen skal diskuterer dagens forberedelse, som er:  
- Introduktion til sikker software udvikling - del 1.  
- Introduktion til sikker software udvikling - del 2.  
- Introduktion til sikker software udvikling - del 3.  
- _Secure by design_ kapitel 1. 

1. Hvad er læren fra Öst-Götha Bank røveriet?
2. Bør sikkerhed tænkes ind i software udvikling som en selvstændig Feature?
3. Hvorfor er det relevant at kende system målene når sikkerhed skal tænkes ind?
4. Hvilket abstraktions niveau(detaljegrad) bør system mål havde?
5. Hvorfor er det en fordel at ulede sikkerheds mål fra system mål?

## Svar
1. Öst-Göta Bank-røveriet var en bankrøveri i 2005 i Sverige. Mens det er vigtigt at bemærke, at jeg ikke har oplysninger om specifikke lærepunkter fra dette røveri, kan bankrøverier generelt illustrere behovet for robuste sikkerhedsforanstaltninger i finansielle institutioner. Det understreger vigtigheden af at have en effektiv sikkerhedsinfrastruktur, uddannelse af personale og samarbejde med retshåndhævelse for at forebygge og håndtere sådanne begivenheder.

2. Ja, sikkerhed bør bestemt tænkes ind som en selvstændig feature i softwareudvikling. Dette koncept kaldes "sikkerhed ved design." Det indebærer at identificere og adressere potentielle sikkerhedsrisici tidligt i udviklingsprocessen snarere end at forsøge at rette dem senere. Ved at betragte sikkerhed som en selvstændig feature kan udviklere identificere og implementere de nødvendige beskyttelsesforanstaltninger og minimere risikoen for sårbarheder.

3. Det er relevant at kende systemmålene, når sikkerhed skal tænkes ind, fordi sikkerheden skal afbalanceres med de overordnede mål for systemet. Dette kaldes ofte sikkerhedsafvejning. Hvis du forstår, hvad systemet er beregnet til at opnå, kan du bedre vurdere, hvilke sikkerhedsforanstaltninger der er nødvendige, og hvor restriktive eller omfattende de bør være. At kende systemmålene hjælper med at undgå overbeskyttelse, der kan hæmme brugernes eller systemets funktionalitet.

4. Abstraktionsniveauet for systemmål bør være tilstrækkeligt detaljeret til at give en klar forståelse af, hvad systemet skal opnå, men ikke så detaljeret, at det begrænser mulighederne for at implementere sikkerhedsforanstaltninger. Normalt udtrykkes systemmål på et højt niveau for at give fleksibilitet i design og implementering.

5. Det er en fordel at adskille sikkerhedsmål fra systemmål, fordi det giver mulighed for en mere objektiv og fokuseret tilgang til sikkerhed. Når sikkerhedsmål er klart defineret og adskilt fra systemmål, kan de prioriteres og implementeres mere effektivt. Desuden hjælper denne adskillelse med at undgå, at sikkerhedsaspekter bliver overset eller underprioriteret i udviklingsprocessen, hvilket kan resultere i sårbarheder og sikkerhedsrisici.