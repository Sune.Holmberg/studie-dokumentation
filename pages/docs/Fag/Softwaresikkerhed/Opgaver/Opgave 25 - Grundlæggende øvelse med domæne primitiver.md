# Øvelse 25 - Grundlæggende øvelse med domæne primitiver.

## Information
Formålet med denne  øvelse er at give en grundlæggende introduktion til domæne primitiver.
Et domæne primitive er en klasse som repræsenter en attribute (Value object i DDD) i kildekoden.
Nedstående billede er en meget simple domæne model noteret med UML Klasse diagram notation.  
![Person domæne model](./Images/PersonDomainModel.jpg)  
Person
- Fornavn
- Efternavn
- Alder
- CPR. nummer
  
I modellen ses der at personen har 4 attributer: fornavn, efternavn, alder og CPR nummer_.
Dette betyder der i koden ville skulle oprettes 4 forskelige klasser for at implementer denne model
med domæne primitiver i koden, altså følgende 4 klasser ville skulle implementeres:  
- Firstname  
- Lastname  
- Age  
- CprNumber  
  
Hver af de 4 klasser skal opretteholde invarians. F.eks. må age ikke være under 0, da ingen mennesker kan være 
under 0 år. Et eksempel er vist herunder:
```Csharp
public class Age{
    private int age;

    public Age(int age){

        this.age = age;
    }

    public int GetValue(){
        return age;
    }

    private void IsAgeValid(int age){
        if(age<0){
            throw new ArgumentException("Age cannot be less then 0");
        }
    }
}
```  
Person klassen har en alder, og vil derfor skulle tage imod et argument som  er af type Age.
```Csharp
public class Person{
        private Age age;

        public Person(Age age){
            if(age != null){
                throw new NullArgumentException("Age cannot be null");
            }

            this.age = age;
        }
    }
```
Da Age opretholder sin egen invarians, og et et objekt af denne type kan derfor ikke oprettes uden at overholde
gældende regler for objekter af denne type, behøver Person klassen behøver blot at kontroller om objektet er null (ukendt).

Domæne primitiver understøtter sikkerheden i software, ved at sikre mod fejlagtige værdier i software. F.eks. vil
et domæne primitive for _fornavn_ der kun tillader alfabetiske karakter aldrig tillade et fornavn som F.eks. `<script>You got hacked</script>`.
  
Alle programmerings sprog har system primitiver. F.eks. har [C#](https://learn.microsoft.com/en-us/dotnet/csharp/language-reference/builtin-types/built-in-types)
blandt andet _int,bool,char double_ og mange flere. Disse understøtter type sikkerheden således at man ikke kan videregive en komma værdi som F.eks `2.4` hvis
typen der skal anvendes er defineret som int(heltal). Dette forhindre dog ikke at værdien kan være negativ et sted hvor det ikke giver mening. F.eks. hvis _age_ blot
er defineret som en int, kan den sagtens defineres som værende -1. Dette forhindre domæne primitiver, ved at definere hvilken værdi alder må havde, inden for et given
domæne. Man implementer altså et type system som understøtter de gældende domæne regler. Dette type system sikre integriteten for de objekter der eksisterer i softwaren,
og øger sikkerhed ved at sikre mod fejlagtige/ondsindet værdier fra F.eks. injection angreb.  

Værktøjer der anvendes til statisk kode analyse, kender ikke til det domæne som software håndtere. Derfor kan automatisk værktøjer såsom
statisk kode analyse ikke finde potentielle sårbarheder ift. domænet. Derfor giver domæne primitiver et ekstra lag af sikkerhed.
  
Ydermere kan man oprette unit test til domæne primitiver(Hvis du ikke ved hvad en unit test er, kommer det senere i undervisningen), hvilket
sikre at domæne regler ikke uhensigtsmæssigt bliver overtrådt, når der foretages ændringer i kildekoden. 



## Instruktioner
1. Opret en ny .Net konsol applikation
2. Opret et domæne primitive for fornavn. Fornavn må kun indeholde alfabetiske karakterer, og skal have en længde på mellem 2 - 20 karakter.
3. Opret en domæne primitive for efternavn. EFternavn må kun indeholde alfabetiske karakterer, og skal have en længde på mellem 2 - 20 karakter.
4. Opret en domæne primitive for alder. Alderen skal være mellem 18 -99
5. Opret en domæne primitive for CPR nummer. CPR nummeret skal overholde syntaksen for et dansk CPR nummer (Kun syntaksen, køn kan du undlade)
6. Opret en _GetValue_ metode i alle Domæne primitiver, således at værdien kan udlæses.(Hvis du ikke allerede har gjort det).
7. Opret en Person klasse der indeholder et _private field_ for hver af de 4 domæne primitiver.
8. Lad _Constructoren: i Person klassen tage imod et argument for hver af de 4 domæne primitiver og brug argumenterne til at initialisere _private fields_
9. Lav en _GetFirstname,GetLastname,GetAge,GetCprNumber_ metode og lad dem returner værdien af domæne primitivet.(Husk og brug GetValue metoden)
10. Opret et Person objekt i konsol applikationen. Og udskriv alle 4 værdier.
11. (Ekstra) CPR nummer er følsomt data. Implementer domænet primitivet til CPR nummer med _Read once pattern_  
_Se Afsnit 5.2 i bogen secure by design._
12. (Ekstra) Lav invariansen i domæne primitivet til Cpr nummer, således at CPR nummeret passer med personen køn.
