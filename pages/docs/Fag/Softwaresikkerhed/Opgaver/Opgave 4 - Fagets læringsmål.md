# Opgave 4 Fagets læringsmål 

## Læringsmål for softwaresikkerhed

### Viden

Den studerende har viden om:

• Trusler mod software 
- Vi skal vide hvad vi skal kunne beskytte software, og hvordan vi skal kunne angribe det

• Kriterier for programkvalitet
- Tjekke om programmerne lever op til standarder

• Fejlhåndtering i programmer
- Man skal kunne analysere og håndtere fejl i programmer 

• Forståelse for security design principles, herunder security by design og privacy by design
- Forståelse af de forskellige principper, og hvordan man kan sikre sin kode

 
### Færdigheder

Den studerende kan tage højde for:

• Programmere håndtering af forventede og uventede fejl
- Reaktiv og proaktiv af håndteringen af koden 

• Definere lovlige og ikke-lovlige input data, bl.a. til test
- Man skal kunne skelne mellem lovlige og ikke-lovlige input data  

• Bruge et API og/eller standard biblioteker
- Man skal kunne bruge forskellgie biblioteker, som bliver brugt som standarder

• Opdage og forhindre sårbarheder i programkoder
- Kodeanalyse

• Sikkerhedsvurdere et givet software arkitektur
- Sikkerhesvurdering

### Kompetencer

Den studerende kan:

• Håndtering risikovurdering af programkode for sårbarheder.
- Kodeanalyse 

• Håndtere udvalgte krypteringstiltag
- Kunne håndtere og kendskab til kryptrografi 


### Læringsmål for Webapplikations sikkerhed


#### Viden
den studerende har viden om:
- Anvendelse af en sikkerhedsstandard til verificering af sikkerhed i web applikationer.
- De hyppigste sårbarheder i web applikationer.
- Anvendte værktøjer til validering af foranstaltninger i web applikationer.e

#### Færdigheder
den studerende kan:
- Udføre grundlæggende tests som validering af implementeret foranstaltninger i web applikationer.
- Udarbejde test cases til validering af implementeret foranstaltninger i web applikationer.
- Anvende en sikkerhedsstandard til at verificer sikkerhed i web applikationer.

#### Kompetencer
Efter fuldførelse af valgfaget kan den studerende:
- Selvstændigt tilegne sig ny viden og færdigheder til identificering af sårbarheder i web applikationer.
- Selvstændig tilegne sig ny viden og færdigheder til implementering af foranstaltninger i web applikationer.
- Opstille kriterier for sikkerhed i en web applikation

### Overlab 
- Trusler mod software
- Sikkerhedsstandarder
- Kendskab til koden 
- Kodeanalyse