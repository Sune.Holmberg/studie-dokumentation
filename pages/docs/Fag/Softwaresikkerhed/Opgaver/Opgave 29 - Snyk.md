# Øvelse 29 - 

## Information
Formålet med denne øvelse er blot at vise at der findes flere værktøjer som kan anvendes til 
skanning for 3. parts sårbarheder. I denne øvelse skal værktøjet Snyk anvendes.

Du skal anvende samme projekt som i forrige øvelse.

## Instruktioner
1.  Du skal installer package manageren `chocolatey`. Følg guide til installation [her](https://chocolatey.org/install)

2.  Brug `chocolatey` til at installer `Snyk CLI`, se guide [her](https://community.chocolatey.org/packages/snyk).
3.  Anvend kommandoen `Snyk test` i projekt folderen.
4.  Samling resultaterne med dotnet vulnerability

![snyk test](../../../billeder/Software-sikkerhed/Opgave%2029%20-%20Snyk%20liste.png)
