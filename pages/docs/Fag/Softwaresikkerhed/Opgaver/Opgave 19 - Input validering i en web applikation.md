# Øvelse 19 - Input validering i en web applikation

## Information
Formålet med denne øvelse er at repeterer input validering og regex, samt at forvise 1 ud af 2 tilgange til
input validering i web applikationer, i denne øvelse anvendes manuelt implementeret input validering.

Fremganggangs måden i denne øvelse er den samme som i øvelse 18.  Denne gang er det dog ikke en konsol applikation
der skal anvende input validering, men en web applikation. Projektet der skal anvendes er `ValidateTheNameWebApplication`.

Applikationen kan eksekveres inde fra projekt folderen med kommandoen `dotnet run`. Hvorefter du i browseren kan navigeres
til applikationens URL adresse og afgive input til validering.

Applikationen har en _Frontend_ som eksekveres i browseren. Den består af en POST form som kan sende data
til applikationen via et HTTP POST request. Når denne data sendes fra browseren til applikationen, overskrider
dataen en tillidsgrænse, og skal derfor valideres. Implementeringen af POST formen kan ses i projekt folderen,
under _Views->Name->Index.cshtml_ . POST formen skal der dog ikke ændres i.

I øvelsen skal du implementerer validerings logik i metoden `NameIsValid` i controller klassen `NameController.cs`,
som kan findes i projekt folderen _Controllers->NameController_, fremgangs måden er identisk med forrige øvelse.

## Instruktioner  
1. Valider at navnet i argumentet `nameInput` ikke er længere 20 karakterer
2. Valider at navnet i argumentet ikke er kortere end 3 karakter
3. Valider at navnet kun indeholder alfabetiske karakter, med Regex (ikke andre tilgange).
4. Sikker dig at validering virker ved at eksekver applikationen, og afprøv med et par forskellige inputs.
_Husk og bibeholde rækkefølgen, således at længden bliver valideret inden i valider tekst strengens indhold_

![Løsning](../../../billeder/Software-sikkerhed/Opgave%2019.png)