# Øvelse 33 - Exception som angrebs vektor.

## Information
Formålet med denne øvelse er at introducerer faren ved manglende håndtering af exceptions i ASP .Net core MVC applikation.
Hvis du skal havde genopfrisket den grundlæggende brug af exceptions i .Net core, kan du med fordel kigge tilbage
på [Øvelse 21](./21_Throwing_Exceptions_In_CSharp.md).


Når en exception bliver kastet i en web applikation, er det vigtigt at den aldrig bliver synlig for slut brugeren(eller andre).
En exception indeholder typisk det der kaldes en _stack trace_ som viser hvilken kode der kastet exception, samt hele kæden
af metode kald der førte til kaldet af metoden, som kastet en exception. _Stack trace_ o.l. indeholder altså information om
softwarens eksekvering samt internt information om softwaren. Dette betyder at en stack trace er en potentiel angrebs vektor,
og kan derfor misbrugs af fjendtlige aktører.

Et godt eksempel på stack trace som angrebs vektor er sql Injection angreb. Typisk vil sådan et 
angreb starte med at angriberen prøver og fremprovoker en fejl i applikationen med enten `'` eller `;` karakter i et tekst felt.
Såfremt der opstår en fejl som følge af disse karakter er det meget sandsynligt at applikationen er sårbar overfor  SQL Injection 
angreb. Hvis den fremprovokeret fejl vises en SQL exception med stacktrace er applikationen helt sikkert sårbar overfor SQL Injection angreb, og
den fjendtlige aktør opnår samtid viden om koden, samt den del af koden som kastet en en exception.


I denne øvelse skal du først fremprovoker en fejl som resulterer i at der vises en stacktrace fra en SQLException. Og herefter skal du
arbejde med håndtering af exception i ASP .Net Core.

Projektet du skal arbejde og klone er [WebGoat](https://github.com/tobyash86/WebGoat.NET).

## Instruktioner
Først skal du verificerer at applikationen virker ved at fortag et køb,
når du så har verificeret applikationens almindelig virke, skal du fremprovokerer en SQL exception.

### Verificer at applikationen virker.
1. Start den klonet applikation.
2. Opret en bruger i applikationen.
3. Køb nogle vare i applikationen (tilføj vare til kurven og bestil dem).

### Fremprovoker en Sql Exception
1. Læg nogle vare i indkøbs kurven.
2. Gå til checkout
3. Udfyld alle felter i Check out. Men i feltet _Name to ship to_ skal du tilføje karakteren `'`.
4. Klik _Place order_
5. Læs exceptionen og stacktrace som bliver vist. Hvilken information får du?
6. Stacktracen viser at linje 52 og linje 151 i kildekoden smed exceptionen. Prøv om du kan finde disse i kilde koden. Hvad er årsagen til at applikationen er sårbar?
7. Gentag trin 1 - 6. Denne gang med `;` i stedet for `'`.

I klassen __OrderRepository__ kan man se årsagen til sårbarheden i metoden _CreateOrder_.
```CSharp
 var sql = "INSERT INTO Orders (" +
                "CustomerId, EmployeeId, OrderDate, RequiredDate, ShippedDate, ShipVia, Freight, ShipName, ShipAddress, " +
                "ShipCity, ShipRegion, ShipPostalCode, ShipCountry" +
                ") VALUES (" +
                $"'{order.CustomerId}','{order.EmployeeId}','{order.OrderDate:yyyy-MM-dd}','{order.RequiredDate:yyyy-MM-dd}'," +
                $"{shippedDate},'{order.ShipVia}','{order.Freight}','{order.ShipName}','{order.ShipAddress}'," +
                $"'{order.ShipCity}','{order.ShipRegion}','{order.ShipPostalCode}','{order.ShipCountry}')";
            sql += ";\nSELECT OrderID FROM Orders ORDER BY OrderID DESC LIMIT 1;";
```
Der bliver anvendt sammentrækning af strenge fra _order_ objekt som er af typen Order. Et hurtig kig i Order klassen afslører 
at klassen ikke håndhæver invarians eller regler af nogen slags. Endevidere har metoden _Checkout_ i Controller klassen _CheckoutController_, som kalder _CreateOrder_ metoden i _OrderRepository_ klassen, ingen input validering. Input validering i Controller klassen, eller brug af  domæne primitiver i Order klassen (og dermed håndhævelse af invarians) ville have kunne mitigeret denne sårbarhed.

Sammentrækning af strenge bør aldrig anvendes til at oprette SQL Forspørgelse. Såfremt det ikke kan undgåes, skal der anvendes [Parameterized statement](https://learn.microsoft.com/en-us/aspnet/web-forms/overview/data-access/accessing-the-database-directly-from-an-aspnet-page/using-parameterized-queries-with-the-sqldatasource-cs). Men den bedste praksis er anvendelse [Object relationel mapper](https://learn.microsoft.com/en-us/dotnet/architecture/modern-web-apps-azure/work-with-data-in-asp-net-core-apps)(ORM) som helt undlader
brugen af tekst strenge.
  
Øverige dele af kildekoden anvender ORM til interaktion med databasen, Nedstående er der vist et eksempel fra metoden _CreateorderPayment_.
```CSharp
var orderPayment = new OrderPayment()
            {
                AmountPaid = Convert.ToDouble(amountPaid),
                CreditCardNumber = creditCardNumber,
                ApprovalCode = approvalCode,
                ExpirationDate = expirationDate,
                OrderId = orderId,
                PaymentDate = DateTime.Now
            };
            _context.OrderPayments.Add(orderPayment);
            _context.SaveChanges();
```
  
ORM bør altid anvendes til interaktion med databaser, fra kildekoden. Der kan undtagelsesvist i meget få tilfælde være en grund
til at undlade at anvende ORM. Dette er dog meget sjældent.
