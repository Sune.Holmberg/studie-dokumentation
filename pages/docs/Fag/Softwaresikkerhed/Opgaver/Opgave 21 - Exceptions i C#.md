# Øvelse 21 - Exceptions i C sharp

## Information
Formålet med denne øvelse er at introducerer Exceptions i c#. Både fordi det skal anvendes når du i næsten
øvelse skal opretholde objekters invariance gennem en constructor. Og fordi du senere skal arbejde med exceptions
i forbindelse med emnet `Fejlhåndtering`.

En exception i C# er en afbrydelse af programmet eksekverings flow. Og er en "Højrøstet" måde at melde om en
fejl. Når du i næste øvelse skal implementer håndhævelsen af invariance med constructor, skal du smide en 
exception hvis der anvendes data som ikke er tilladt. Dog skal du i denne øvelse først havde en introduktion
til hvad en Exception er. 

Den [Tutorial du skal følge kan findes her](https://www.csharptutorial.net/csharp-tutorial/csharp-throw-exception/)

Yderlige information om Exceptions i C# kan du finde [Her](https://learn.microsoft.com/en-us/dotnet/csharp/fundamentals/exceptions/creating-and-throwing-exceptions).

## Instruktioner
1. Opret en ny .Net konsol applikation
2. Følg og implementer tutorialen.
3. Prøv at oprette `Circle objekt` med negativ radius. Samt fjerne `Try` & `Catch` fra koden. (Dette vil lave det som hedder en _Uncaugth exception_ )
