# Øvelse 15 - Forstå koden

## Information
Formålet med denne øvelse er opnå en forståelse for samehængen mellem
implementeret kode og det domæne koden arbejder indenfor.

Repositoriet du skal anvende i denne øvelse kan findes her:
[https://github.com/mesn1985/missingDomain](https://github.com/mesn1985/missingDomain)


## Instruktioner  
1. klone repositoriet.
2. Gennemgå koden og forsøg at forstå koden.
3. Vurder om koden er sikker.

## Løsning 
1. 
2. - BA klassen
```namespace space1;

public class BA
{
    private static int ACN = 1;

    public string NUM { get; }
    public string Owner { get; set; }

    private List<Tr> _at = new List<Tr>();

    public decimal bal
    {
        get
            {
                decimal bal = 0;
                foreach (var item in _at)
                {
                    bal += item.Am;
                }
                return bal;
            }
    }
    public BA(string nm, decimal ib)
    {
        NUM = ACN.ToString();
        ACN++;

        this.Owner = nm;
        Md(ib, DateTime.Now, "Initial");

        
    }
    public void Md(decimal am, DateTime d, string n)
    {
        if (am <= 0)
        {
            throw new ArgumentOutOfRangeException(nameof(am), "Amount must be positive");
        }
        var dep = new Tr(am, d, n);
        _at.Add(dep);
    }

    public void Mw(decimal a, DateTime d, string n)
    {
        if (a <= 0)
        {
            throw new ArgumentOutOfRangeException(nameof(a), "Amount must be positive");
        }
        if (bal - a < 0)
        {
            throw new InvalidOperationException("ba is insufficient");
        }
        var wd = new Tr(-a, d, n);
        _at.Add(wd);
    }
    public string Gac()
    {
        var report = new System.Text.StringBuilder();

        decimal bal2 = 0;
        report.AppendLine("D\t\tA\tB\tN");
        foreach (var i in _at)
        {
            bal2 += i.Am;
            report.AppendLine($"{i.D.ToShortDateString()}\t{i.Am}\t{bal2}\t{i.N}");
        }

        return report.ToString();
    }
}
```

- Program klassen
```using space1;

var acc = new BA($"<script>alert('hej')</script>", 1000);
acc.Md(500, DateTime.Now, "MD made");
Console.WriteLine(acc.bal);
acc.Md(100, DateTime.Now, "Test message");
Console.WriteLine(acc.bal);
Console.WriteLine(acc.Gac());
try
{
    acc.Mw(750, DateTime.Now, "Attempt to od");
}
catch (InvalidOperationException e)
{
    Console.WriteLine("Exception caught trying to od");
    Console.WriteLine(e.ToString());
}
BA ia;
try
{
    ia = new BA("test", 55);
}
catch (ArgumentOutOfRangeException e)
{
    Console.WriteLine("Exception caught creating with negative value");
    Console.WriteLine(e.ToString());
    return;
}
```
- Tr klassen 
```
namespace space1;

public class Tr
{
    public decimal Am { get; }
    public DateTime D { get; }
    public string N { get; }

    public Tr(decimal a, DateTime d, string n)
    {
        Am = a;
        D = d;
        N = n;
    }
}
```

3. Det er ikke sikkert, fordi der ikke er indputvalidering på strings (Man kan indsætte scripts)

![BA](../../../billeder/Software-sikkerhed/Opgave%2015%20-%20ikke%20sikker%20string.png)

Billedet viser dele af constructoeren inde fra BA Objectet

En anden ting som gør det usikkert, er at der er dårlige variablenavne i koden (Hvis man ikke kan hurtigt se hvad metoderne skal kunne = dårlig kode)
