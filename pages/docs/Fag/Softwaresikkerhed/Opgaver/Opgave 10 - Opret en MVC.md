# Øvelse 10 - Opret en applikation med ASP .Net core.

## Information
I denne øvelse skal i lave et REST API med .NET frameworket ASP .NET Core.
Formålet med øvelsen er at give en grundlægende introduktion til strukturen i
API applikationer, samt hvordan der indledningsvist kan interageres med disse.

Hvis det er første gang man arbejder med et større framework kan det godt virker
meget uoverskueligt. Det er helt naturligt, og det er helt okay. Fokuser på at følge
tutorialen. Både mens du udfører tutorialen, og efter. Kan du finde vejledning og vigtige
pointer under instruktioner, samt afsnittes overordnet inhold.

**Husk der er en læringskurve, så du møder mange nye begreber og termanologier, tanken er ikke at du skal kunne dem alle lige nu, men løbende henover semesteret!**

Tutorialen der skal udføres er denne: [Tutorial: Create a web API with ASP.NET core](https://learn.microsoft.com/en-us/aspnet/core/tutorials/first-web-api?view=aspnetcore-7.0&tabs=visual-studio-code)

**Stop tutorialen når du når til afsnittet Prevent over-posting**  
**Når du har implementeret tutorialen(og den virker), så gå igennem koden og se hvor meget du kan genkende(Klasser, Objekter, metoder osv.)**
**Vi gennemgår applikationen næste undervisningsgang**

## Instruktioner  
Herunder finder du kommentarer og vigtige pointer fra hvert afsnit.  

### Overview
Bemærk at alle [URL](https://developer.mozilla.org/en-US/docs/Learn/Common_questions/Web_mechanics/What_is_a_URL) paths der kan bruges med applikationen specificeres i dettes afsnit.
Herudover vises et diagram applikationen overordenet arkitektur. Bemærk at Client er en anden applikation.

### Create project
Her kan du se hvordan du autogenerater den indledende projekt opsætning.
Bemærk at du kan vælge hvilken værktøjer du vil se instrukser for (VS code eller Visual studio)

### Test the project
Her findes instrukserne til at afprøve projektet efter autogenering.
Bemærk at der skal generes et self-signed TLS certifikat for at kunne anvende HTTPS.

Herudover viser afsnittet også hvilken format appliaktionen leveres i. Dette er [JSON](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Objects/JSON) formattet.

### Add a model class
I dette afsnit skal du lave et klasse som definer den type der skal repræsentere _TodoIem_ i koden.
Er der nogen regler for hvordan objekter af denne type kan oprettes når der ikke anvendes en Construtor, eller andre begrænsninger?

### Add a database context
I dette afsnit oprettes der en klasse til håndtering af database interaction.
Dette er fordi applikationen benytter sig af en såkaldt [ORM](https://www.entityframeworktutorial.net/what-is-entityframework.aspx?utm_content=cmp-true).
.Net core's ORM hedder Entity framework.  
**Det er helt ok hvis du ikke kan gennemskue hvor Database interaktionen fungere lige nu, du skal blot implementerer det, så snakker vi om det næste lektion**

### Add a database context
I dette afsnit laves konfigurationen som fortæller databasen hvilken klasse den skal benytte sig af for at 
interagere med databasen. (Den database kontekts det blev lavet i forrige afsnit). Der anvendes noget som hedder
dependecy injektion(Det er ikke en forventning af du har en forståelse for dette lige nu).

### Scaffold a controller.
I dette afsnit skal du tilføje nogle 3. parts bibloteker fra Microsoft. Disse bibloteker anvender CLI værktøjet _Dotnet_ til 
at autogenere en såkaldt _Controller_. Controllers er typisk de klasser der håndter udefrakommende interaktion.

### Update the PostTodoItem create method
I dette afsnit skal du ændre en metode som blev autogenereret i forrige trin.
Metoden _PostTodoItem_ er metoden som bruges til at håndtere [HTTP Request](https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods)
af typen POST. HTTP POST metoden bruges når man vil oprette ny data i applikationen.

### Test PostToDoItem
I dette afsnit skal du afprøve api'et ved at ændre i JSON teksten.  
**Det er en smule uklart i tutorialen, men du skal klikke på "Try it out" for få menuen, hvor du kan ændre i teksten**

### Øverige afsnit indtil Prevent over-posting
De følgende afsnit er gennem metoderne der håndter forskellige HTTP requests.
De er gode at  læse for at skabe overblik.

## Løsning

### Program.cs
```
using Microsoft.EntityFrameworkCore;
using Opgave_10___Opret_MVC.Models;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
builder.Services.AddDbContext<TodoContext>(opt =>
    opt.UseInMemoryDatabase("TodoList"));
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();

```

### TodoItemsController.cs
```
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Opgave_10___Opret_MVC.Models;

namespace Opgave_10___Opret_MVC.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TodoItemsController : ControllerBase
    {
        private readonly TodoContext _context;

        public TodoItemsController(TodoContext context)
        {
            _context = context;
        }

        // GET: api/TodoItems
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TodoItem>>> GetTodoItems()
        {
          if (_context.TodoItems == null)
          {
              return NotFound();
          }
            return await _context.TodoItems.ToListAsync();
        }

        // GET: api/TodoItems/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TodoItem>> GetTodoItem(long id)
        {
          if (_context.TodoItems == null)
          {
              return NotFound();
          }
            var todoItem = await _context.TodoItems.FindAsync(id);

            if (todoItem == null)
            {
                return NotFound();
            }

            return todoItem;
        }

        // PUT: api/TodoItems/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTodoItem(long id, TodoItem todoItem)
        {
            if (id != todoItem.Id)
            {
                return BadRequest();
            }

            _context.Entry(todoItem).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TodoItemExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        [HttpPost]
        public async Task<ActionResult<TodoItem>> PostTodoItem(TodoItem todoItem)
        {
            _context.TodoItems.Add(todoItem);
            await _context.SaveChangesAsync();

            //    return CreatedAtAction("GetTodoItem", new { id = todoItem.Id }, todoItem);
            return CreatedAtAction(nameof(GetTodoItem), new { id = todoItem.Id }, todoItem);
        }

        // DELETE: api/TodoItems/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTodoItem(long id)
        {
            if (_context.TodoItems == null)
            {
                return NotFound();
            }
            var todoItem = await _context.TodoItems.FindAsync(id);
            if (todoItem == null)
            {
                return NotFound();
            }

            _context.TodoItems.Remove(todoItem);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool TodoItemExists(long id)
        {
            return (_context.TodoItems?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}

```

### TodoContext.cs

```
using Microsoft.EntityFrameworkCore;

namespace Opgave_10___Opret_MVC.Models
{
    public class TodoContext : DbContext
    {
        public TodoContext(DbContextOptions<TodoContext> options)
            : base(options)
        {
        }

        public DbSet<TodoItem> TodoItems { get; set; } = null!;
    }
}

```

### TodoItems.cs

```
namespace Opgave_10___Opret_MVC.Models
{
    public class TodoItem
    {
        public long Id { get; set; }
        public string? Name { get; set; }
        public bool IsComplete { get; set; }
    }
}

```