# Øvelse 22 - Opretholdelse af invariance med constructoren

## Information
Formålet med denne øvelse er at give en grundlæggende introduktion til håndhævelse af invariance gennem constructoren.

Ved at definerer regler for hvilken data et objekt må indeholde, og at sikre sig at objekter ikke kan oprettes uden at
overholde disse regler(Håndhævelse af invariance), kan andre dele af softwaren altid kan stole på at objektet
over de regler som er gældende indenfor et enkelt domæne. Man sikre sig altså at objekter der eksisterer i softwaren opretholder
sin integritet.

Du kan få lidt vejledning ift. private fields og constructors [her](https://learn.microsoft.com/en-us/dotnet/csharp/programming-guide/classes-and-structs/constructors)
vær dog opmærksom på at ikke anvender metoder, som du skal anvende i trin 7 & 8.

Immutability betyder at når først et objekt er oprettede, så kan der ikke ændres på det. Dette betyder at et objekt som har håndhævet
invarians ved oprettelsen og er immutable, vil man altid kunne stole på, kun indeholder data jvf. opstillet regler. Derfor er anvendelsen af private
fields vigtig.

## Instruktioner
1. Opret en ny konsol applikation i C#.
2. Opret en ny klasse ved navn Person.
3. Lav et [private field](https://learn.microsoft.com/en-us/dotnet/csharp/programming-guide/classes-and-structs/fields) i klassen ved navn `firstname` af typen string.
4. Lav et [private field](https://learn.microsoft.com/en-us/dotnet/csharp/programming-guide/classes-and-structs/fields) i klassen ved navn `lastname` af typen string.
5. Lav klassens [constructor](https://learn.microsoft.com/en-us/dotnet/csharp/programming-guide/classes-and-structs/constructors) og lad constructoren tage imod to string argumenter, og bruge disse to til at initialiserer de 2 private fields.
6. Håndhæv invariancen for objektet ved at valider både firstname og lastname argumentet i constructoren. Ingen af de 2 må være `null`. Og reglerne for begge det samme som i[Opgave 18](18_InputValideringKonsolApplikation.md). Der skal smides en exception hvis reglerne ikke er overholdt.
7. Lav en metode i klassen ved navn `GetFirstName`, den skal retunerer værdien fra private field firstname.
8. Lav en metode i klassen ved navn `GetLastName`, den skal retunerer værdien fra private field lastname.
9.  Lav en instans af objektet med valid fornavn og efternavn.
10. Udskriv fornavn og efternavn i konsollen.

