# Øvelse 30 - Grundlæggende taint analyse med Snyk.

## Information
Formålet med denne øvelse er at introducerer grundlæggende taint analyse med med 
værktøjet [Snyk](https://docs.snyk.io/scan-applications/snyk-code/using-snyk-code-from-the-cli).

I opgaven skal du bruge udføre analysen på en ASP .Net core applikation. Repositoriet du skal 
klone er her: [https://github.com/brunobritodev/ASP.NET-Core-SQL-Injection-Example](https://github.com/brunobritodev/ASP.NET-Core-SQL-Injection-Example)  
Som navnet på applikationen indikerer, indeholder applikationen en sårbarhed der gør den modtagelig overfor injection angrebet _SQL Inject_.
Sårbarheden som vi skal arbejde med, er implementeret i Controller klassen `Homecontroller`, i metoden `SearchStudentUnsecure`.
Sårbarheden er meget åbenlys, hvilket gør den velegnet til at lære om taint analyser.

Taint analysen udføres ved at en algoritme følger en input værdi fra kilden (source) og til dens endelige mål(sink).
I applikationen er source, browserens input til applikationens, som ses i metoden ```public async Task<IActionResult> SearchStudentUnsecure(string name)```.
source må altså være argumentet ```name```. Sink i applikationen er databasen hvilket også kan ses i metoden `SearchStudentUnsecure` koden:
```Csharp
            var conn = _context.Database.GetDbConnection();
            var query = "SELECT FirstName, LastName FROM Student WHERE FirstName Like '%" + name + "%'";
            IEnumerable<Student> students;

            try
            {
                await conn.OpenAsync();
                students = await conn.QueryAsync<Student>(query);
            }
```
Kode linjen `await conn.QueryAsync<Student>(query)` laver et kald til databasen med en SQL string som indeholder den 
ikke valideret data. Det vil sige at _tainted_ data nu får lov til at bevæge sige direkte fra source til sink.

I opgaven skal i efterprøve dette ved at anvende værktøj Snyk CLI som taint analyse værktøj.

Værktøjet er det samme i anvendte i [Opgave 29](29_Vulnerable_dependencies_Snyk.md).  
Du skal dog først oprette en konto og tænde for `Code scan` funktionaliteten. Følg guiden [her](https://docs.snyk.io/scan-applications/snyk-code/using-snyk-code-from-the-cli)
Efterfølgende skal du blot følge trine i instruktionerne.

## Instruktioner
1. I det klonet projekts folder. Skift til folderen `SqlInjection`
2. udfør taint analysen ved at følge instruktionerne fra afsnittet `Testing a repository from its root folder` i denne [dokumentation](https://docs.snyk.io/scan-applications/snyk-code/using-snyk-code-from-the-cli/testing-your-source-code-using-the-cli)
3. Noter dig hvilke linje nummer SQL inject sårbarhed bliver fundet på.

![billed](../../../billeder/Software-sikkerhed/Opgave%2030%20-%20snyk%20code.png)

4. Verificer i koden, at linje nummeret for sårbarheden er der hvor den forurenet data sendes til sink.

- Det gør den 