# Detekterings 7 abstraktions lag

## Features Selection 
- Prøve at stjæle adgangskoder

## Features Extraction
- Igennem auth.log

## Event Selection
- Stor forøgelse i login entries 

## Event detection
- Man holder øje med logs, og ser efter om der er sket et spike eller andet mistænksomt

## Attack detection 
- Man sætter nogen værdier, for at så man kan holde flaske positiver 
- Eksempelvis man at alarmen kun går af hvis der er sket 5 eller mere login forsøg

## Attack classification
- Man ser på hvad det er for en type angreb
- Man kan se der har været en del forsøg 

## Attack Alarming
- Det bliver vist på de platforme som virksomheden bruger, 
- eks. der kan blive sendt en mail til administratoren eller den person som angrebet er sket imod.