# Opgave 18 - Applikations logs

1. installer apache2 web server med kommandoen "sudo apt-get install apache2"
- Jeg kunne ikke få det til at virke på en Ubuntu masking, så jeg gjorde det på en kali i stedet.

2. Verificer at servicen er startet med kommandoen "systemctl status apache2"
![Billed](../../../billeder/System-sikkerhed/opg%2018%20-%20systemctl%20status%20apache2.png)

3. I en browser, gå ind på url'en . Verificer at det er apache's default side der kommer frem
- Man trykker på linket som står under Docs på billedet over.

4. Udskriv indholdet af apache's adgangs log som findes i "/var/log/apache2/access.log"
- For at kunne udskrive log filen, skal man bruge kommandoen "sudo tail -f /var/log/apache2/access.log" 
- Jeg har kan dog ikke få det til at virke på på nuværende tidspunkt 

5. Verificerer at den sidste log i filen viser at den sidste maskine som har tilgået web serveren er den som du brugte til at udføre trin 3

6. Eksekver kommandoen "tail -f access.log"

7. Udfør trin 3 et par gange igen, og verificer at der kommer en ny log entry hvergang.