# Opgave 28 - audit et directory

1. Opret et nyt directory
- "mkdir <navn på directory>"

2. Opret en audit regel med kommandoen "auditctl -w <Directory path> -k directory_watch_rule"
- ![ny dir](../../../billeder/System-sikkerhed/nyt%20dir%20opg%2028.png)

3. Bemærk at permission er bevist undladt

4. Brug auditctl til at udskrive reglen. Bemærk hvilken rettigheder der overvåges på. Dette er pga.´-p´ muligheden blev undladt
![ny dir](../../../billeder/System-sikkerhed/nyt%20dir%20opg%2028.png)

5. Brug "chown" til at give "root" ejerskab over directory(fra trin 1), og brug "chmod" til at give "root" fuld rettighed og alle andre skal ingen rettigheder havde.

6. Med en bruger som ikke er "root", eksekver kommandoen "ls <Directory path>" (directory er fra trin 1)

7. eksekver kommandoen "ausearch -i -k directory_watch_rule"
![ausearch](../../../billeder/System-sikkerhed/ausearch%20opg%2028.png)