Opgaver Bruger rettigheder
- For at redigere i en fil bruges "chmod rwx filnavn"
- chmod -rwx mappenavn for at fjerne tilladelser.
- chmod x filnavn for at tillade eksekverbare tilladelser.
- chmod -wx filnavn for at fjerne skrive- og eksekverbare tilladelser.

Skift ejer af en fil
- chown USER FILE - eks "chown s file1.txt"