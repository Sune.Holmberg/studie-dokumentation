# Genstart af logning i Linux 

- Brug "service rsyslog stop" til at stoppe logningen 
- Brug "service rsyslog start" til at genstarte logningen 

- Man skal dog være logget ind som en sudo user for at kunne genstarte logning