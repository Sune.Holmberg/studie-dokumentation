# Opgave 19 - Linux installing iptables

1. Installer iptables med kommandoen "sudo apt install iptables"

2. Udskriv version nummeret med kommandoen "sudo iptables -V"

3. Installer iptables-persistent med kommandoen "sudo apt install iptables-persistent"

4. Udskriv alle firewall regler(Regel kæden) med kommandoen "sudo iptables -L"

![Firewall](../../../billeder/System-sikkerhed/Opgave%2019%20-%20firewall%20regler.png)

5. Reflekter over hvad der menes med firewall regler, samt hvad der menes med "regel kæden".
- Regler bestemmer over hvad firewallen skal lukke ind, og hvad det er for nogen ting som den skal kassere
- Regel kæden fortæller om rækkefølger på de forskellige regler

6. Når man har ændret en iptables firewall regel, skal man manuelt gemme den nuværende regel opsætning med kommandoen "sudo netfilter-persistent save" ellers er regel listen tom efter genstart