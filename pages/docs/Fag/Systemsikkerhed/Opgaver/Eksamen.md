# Eksamen

- Selve præsentationen er valgfri hvad der skal med i den.
- Det behøver nødvendigvis ikke at have noget med rapporten at gøre.
- Vigtigt kun at have ting med i præsentationen, som man er sikker på at man kan svare på, inde til eksamen.
- Det er en god ide at have konkrette eksempler med i præsentationen
- De kan spørge ind til alt som er blevet lavet om i semesteret (læringsmål og områder), men som udgangspunkt vil de spørge ind til præsentationen og rapporten.
- Selve tiden til præsentationen er lidt irrelevant, hvis man går over de 10 min, så betyder det bare at underviser og censor må begynde at stille spørgsmål.
- Husk ikke at overkomplicere tingene

## Eksamens forberedelse opgave
- Hvilken nogle emner har arbejdet med i undervisningen?
    - Grundlæggende intro til OS-systemer og Linux
    - Logging, brugersystemer og adgangskontrol
    - Firewalls, host based firewall
    - Fysik sikkerhed
    - Forensencics
    - CIS benchmarchens
    - Risikoanalyse
    - Threat modeling 
    - Overvågning og SIEM systemer
    - Malware og antivirus
    - Kryptering
    - Active Directory og windows 
    - CVE og CWE + Opdatering

- Hvilken nogle læringsmål knytter sig til hver emne?
    - Grundlæggende intro til OS-systemer og Linux
        - Kompetancer og håndtering af kommandoline 

    - Logging, brugersystemer og adgangskontrol
        - Udnytte modforanstaltninger til sikring af systemer
        - Implementere systematisk logning og monitering af enheder
        - OS roller ift. sikkerhedsovervejelser
        - Håndtere udvælgelse, anvendelse og implementering af
     praktiske mekanismer til at forhindre, detektere og reagere over for specifikke
     it-sikkerhedsmæssige hændelser
        - Analysere logs for incidents og følge et revisionsspor
        - Væsentlige forensic processer
        - Generelle governance principper / sikkerhedsprocedurer
        - Kan genoprette systemer efter en hændelse

    - Firewalls, host based firewall
        - Relevante it-trusler
        - Udnytte modforanstaltninger til sikring af systemer
        - Håndtere udvælgelse, anvendelse og implementering af
     praktiske mekanismer til at forhindre, detektere og reagere over for specifikke
     it-sikkerhedsmæssige hændelser.
        - håndtere værktøjer til at identificere og fjerne/afbøde forskellige typer af endpoint trusler
        - håndtere enheder på command line-niveau
        - Analysere logs for incidents og følge et revisionsspor
        - Relevante sikkerhedsprincipper til systemsikkerhed

    - Forensencics
        - Væsentlige forensic processer
        - Analysere logs for incidents og følge et revisionsspor
        - Relevante it-trusler
        - Relevante sikkerhedsprincipper til systemsikkerhed
        -  Udnytte modforanstaltninger til sikring af systemer
        - Implementere systematisk logning og monitering af enheder
        - OS roller ift. sikkerhedsovervejelser
        - Håndtere udvælgelse, anvendelse og implementering af
     praktiske mekanismer til at forhindre, detektere og reagere over for specifikke
     it-sikkerhedsmæssige hændelser
        - Analysere logs for incidents og følge et revisionsspor
        - Væsentlige forensic processer
        - Generelle governance principper / sikkerhedsprocedurer

    - CIS benchmarchens
        - Generelle governance principper / sikkerhedsprocedurer
        - Følge et benchmark til at sikre opsætning af enhederne
        - håndtere enheder på command line-niveau
        - Håndtere udvælgelse, anvendelse og implementering af
     praktiske mekanismer til at forhindre, detektere og reagere over for specifikke
     it-sikkerhedsmæssige hændelser.
        
    - Risikoanalyse og Threat modeling 
        - Generelle governance principper / sikkerhedsprocedurer
        - Væsentlige forensic processer
        - Relevante it-trusler
        - Relevante sikkerhedsprincipper til systemsikkerhed
        - OS roller ift. sikkerhedsovervejelser
        -  Håndtere udvælgelse, anvendelse og implementering af
     praktiske mekanismer til at forhindre, detektere og reagere over for specifikke
     it-sikkerhedsmæssige hændelser.
        - håndtere værktøjer til at identificere og fjerne/afbøde forskellige typer af endpoint trusler
    
    - Overvågning og SIEM systemer
        - Udnytte modforanstaltninger til sikring af systemer
        - Implementere systematisk logning og monitering af enheder
        - OS roller ift. sikkerhedsovervejelser
        - Håndtere udvælgelse, anvendelse og implementering af
     praktiske mekanismer til at forhindre, detektere og reagere over for specifikke
     it-sikkerhedsmæssige hændelser
        - Analysere logs for incidents og følge et revisionsspor
        - Væsentlige forensic processer
        - Generelle governance principper / sikkerhedsprocedurer
        - Sikkerhedsadministration i DBMS 

    - Malware og antivirus
        - Relevante it-trusler
        - Sikkerhedsadministration i DBMS
        - OS roller ift. sikkerhedsovervejelser
        - Følge et benchmark til at sikre opsætning af enhederne
        - håndtere enheder på command line-niveau
        - Håndtere udvælgelse, anvendelse og implementering af
     praktiske mekanismer til at forhindre, detektere og reagere over for specifikke
     it-sikkerhedsmæssige hændelser.
        - håndtere værktøjer til at identificere og fjerne/afbøde forskellige typer af endpoint trusler
        - håndtere relevante krypteringstiltag

    - Kryptering
        - håndtere relevante krypteringstiltag
        - Relevante it-trusler

    - Active Directory og windows 
        - OS roller ift. sikkerhedsovervejelser
        - Følge et benchmark til at sikre opsætning af enhederne
        - Håndtere udvælgelse, anvendelse og implementering af
     praktiske mekanismer til at forhindre, detektere og reagere over for specifikke
     it-sikkerhedsmæssige hændelser.
        - Udnytte modforanstaltninger til sikring af systemer
        - håndtere værktøjer til at identificere og fjerne/afbøde forskellige typer af endpoint trusler

    - CVE og CWE + Opdatering
        - Væsentlige forensic processer
        - Relevante it-trusler

- Hvilken nogle læringsmål dækker i med jeres semesterprojekt?
    - Generelle governance principper / sikkerhedsprocedurer
    - Væsentlige forensic processer
    - Relevante it-trusler
    - Relevante sikkerhedsprincipper til systemsikkerhed
    - Sikkerhedsadministration i DBMS.

    - Følge et benchmark til at sikre opsætning af enhederne
    - Implementere systematisk logning og monitering af enheder
    - Analysere logs for incidents og følge et revisionsspor
    - Kan genoprette systemer efter en hændelse.

    - håndtere enheder på command line-niveau
    - håndtere værktøjer til at identificere og fjerne/afbøde forskellige typer af endpoint trusler
    - Håndtere udvælgelse, anvendelse og implementering af
     praktiske mekanismer til at forhindre, detektere og reagere over for specifikke
     it-sikkerhedsmæssige hændelser.
    - håndtere relevante krypteringstiltag

- Hvilken nogle eksempler kan i bruge i rapporten?
    - Eksempler fra Logs, Netværksdiagram, Risikoanalyse, opsætning af firewalls, IDS/IPS og SIEM-systemer, beredskabsplan

- Hvilken nogle eksempler kan i bruge til eksamens præsentationen?
    - Eksempler fra firewalls, beredskabsplanen, netværksdiagram og risikoanalysen 

- Hvilken nogle eksempler kan i bruge fra andre fag?
    - Netværksdiagram, IDS/IPS, beredskabsplan, governance, risikoanalyse, netværksopsætning