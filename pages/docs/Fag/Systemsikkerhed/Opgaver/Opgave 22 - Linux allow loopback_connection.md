# Opgave 22 - Linux allow loopback_connection

1. tillad forbindelser fra loopback interfacet med kommandoen "sudo iptables -I INPUT 1 -i lo -j ACCEPT"

2. Udover at Linux bruger loopback til interne operationer, så er input fra loopback også en stor hjælp ift. evt. fejlfinding på applikationer.