# Opgave 23 - Linux allow specific ICMP messages

1. tillad ICMP pakker af type 3 med kommandoen "sudo iptables -A INPUT -m conntrack -p icmp --icmp-type 3 --ctstate NEW,ESTABLISHED,RELATED -j ACCEPT"

2. tillad ICMP pakker af type 11 med kommandoen "sudo iptables -A INPUT -m conntrack -p icmp --icmp-type 11 --ctstate NEW,ESTABLISHED,RELATED -j ACCEPT"

3. tillad ICMP pakker af type 12 med kommandoen "sudo iptables -A INPUT -m conntrack -p icmp --icmp-type 12 --ctstate NEW,ESTABLISHED,RELATED -j ACCEPT"

4. De ICMP pakker der er blevet tilladt, er hver især af en bestemt type. Undersøg hvad hver af de 3 typer betyder.
- Den første ICMP pakke, laver en "Destination unreachable" situation
- Den anden ICMP pakke, laver en "Time exceeded" situation 
- Den sidste ICMP pakke, laver en  "Paramater problem" 
- Se en liste af de forskellige ICMP pakker [her](https://www.iana.org/assignments/icmp-parameters/icmp-parameters.xhtml)