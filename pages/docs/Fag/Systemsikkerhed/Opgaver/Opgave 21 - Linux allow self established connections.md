# Opgave 21 - Linux allow self established connections

1. Eksekver kommandoen "sudo iptables -I INPUT -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT"

2. Udskriv reglen listen.
![Listen](../../../billeder/System-sikkerhed/Opgave%2021.png)


I kommandoen vælger vi at indsætte reglen først i regelen kæden med "-I" muligheden. Herefter vælges det er et iptables module med "-m conntrack" (conntrack==connection track) for at iptables kan holde styr på hvilken forbindelser der er etableret, og af hvem. Til sidst bruges muligheden "--ctstate ESTABLISHED,RELATED -j ACCEPT" som tillader indadgående pakker fra serverer hvor klienten(OS) selv har oprettet forbindelse.