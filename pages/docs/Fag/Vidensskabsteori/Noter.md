# Noter

## Hypoteser 
- En forløbig antagelse af om det resultat som man vil komme frem til i sin undersøge

### Symmetriske hypotese

### Asymmetriske hypotese

## Belæg
- Belægget er det, der forklarer påstanden

## Hjemmel
- Hjemlen er det, der gør belægget gyldigt, altså den sammenhæng, der må eksistere mellem påstand og belæg

## Sekundære undersøgelser 
- Undersøgelser som ikke er fortaget af forfatteren

## Primære undersøgelser
- Undersøgelser som er fortaget af forfatteren 


