# Fagbegreber 

Induktion
- Ikke nødvendigvis sand
- Man observere et tilstrækkeligt antal tilfælde eller eksempler og udlede en generel regel
- Generalisering

Deduktion
- Altid sande hvis teori er sand og kan bevises
- Man tager en allerede eksisteren teori, og bruger den til at drage konklusioner

Empirisk data
- Undersøgelses data, som kan reflekteres til verden 
- Dataen som understøtter ens augumenter 

Rudolf Carnap
- Forgænger for logisk positivisme
- Mener at vidden kommer af en kombiantion af observationer og teoretisk formulering

Karl Popper
- Beviser sin teori ved at prøve at afbekræfte den
- Falsifikatisme 

Bruno Latour
- Videnskabelig viden er ikke blot et resultat af objektive observationer, men er formet af sociale, kulturelle og politiske faktorer
-  Man skal være opmærksom på sin egen baggrund
- Troværdighed kommer ud fra det man har lavet

Bitsch-Olsen og Pedersen
- Problemformuleringen åbner for research 

Wilhelm Dilthey
- Man kan ikke sætte mennesker i en bås, som man kan med eksperimenter

Hans-Georg Gadamer
- Læseren og teksten har deres egen forståelse af teksten
- Tiden man læser teksten på, kan påvirke forståelse
- Tidliger tekster man har læst, kan være med til at ændre ens forståelse
- Måden tingene er skrevet på, kan påvirke forståelsen

Stephen Toulmin
- Skaber af den treledets augementationsmodel

Pierre Bourdieu
- Troværdighed kommer fra hvor man står i det sociale

Naturvidenskab
- Beskæftiger sig med observerbare og målbare fænomener i den ydre verden
- Alt hvad man kan regne på

Humanvidenskab
- Man vender blikket indad 
- Man observere menesker ud fra deres adfærd og bevidsthed 
- humanvidenskaberne skulle fokusere på at fange den subjektive mening og det meningsfulde mønster i menneskelige handlinger og kulturelle udtryk (Generaliserer)

Logisk positivisme
- Der skal være belæg for alt, ellers er det en personlig mening som er ligegyldig
- En påstand er kun sand, hvis de betingelser, der gør den sandt eller falsk, kan præsenteres tydeligt

Kritisk rationalisme
- Den kritiske rationalisme fokuserer på vigtigheden af kritisk tænkning, falsifikation og objektivitet i videnskabelig og intellektuel undersøgelse

Falsifikation
- Man går efter at kunne bevis at påstanden ikke er sand, ved at gå efter at modbevise den

Positivistisk syn
- Forskeren er neutral, og ikke aktivt involvere sig
- Ikke lade sig påvirke af andre 
- Dataindsamlingen er kvantitativ

Fænomenologisk (hermeneutisk) syn
- Forskeren er en aktiv del af undersøgelsen
- Dataindsamlingen er kvalitativ

Videnskab 
- Man kan bevise om det er sandt eller ej 

Pseudovidenskab
- Ikke falsificerbar
- Man kan ikke bevise om det er sandt eller ej
- Eksempelvis med Astrologi, hvor man udfra stjerne kan se efter hvordan man kommer til at være


Asymetrisk hypotese
- Alle påstande indeholder deres modsætninger: påstanden er en benægtelse af en anden påstand, der understøtter falsifikationen
- Asymmetrisk hypotese er en måde at teste forudsigelser og sammenhængen mellem variabler, der undersøges


![Overblik](../../billeder/Videnskabsteori/Sk%C3%A6rmbillede%202023-05-31%20224938.png)